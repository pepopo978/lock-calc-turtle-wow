const csv = require('csvtojson')
const fs = require('fs')
import Effects from '../wow/interface/Effects'
import WeaponStats from './interface/WeaponStats'
import ItemJSON, { ObtainType, PVPLocations, WorldBoss } from './interface/ItemJSON'
import ItemSlot from './enum/ItemSlot'
import TargetType from './enum/TargetType'

const outputPath = './src/wow/db/lockItems.json'
const allowableClassesRegex = /Allowable Classes: (.*)</m

const ArcanistRegalia5SetSpellPenCorrection = 10

// spell pen values on turtle db are all "1"
const spellPenCorrections: Record<string, number> = {
  Sageblade: 10,
  'Sealbreaker Staff': 20,
  Soulseeker: 25,
  'Enigma Robes': 20,
  'Frostfire Robe': 13,
  'Gem of Nerubis': 10,
  'Mindpiercer Band': 10, // this is a guess
  'Auspicious Ring of the Seer': 10,
  'Don Rodrigo\'s Band': 20,
  'Ring of Swarming Thought': 20,
  'Shawl of End Times': 13,
  'Shroud of the Archlich': 10,
  'Veil of Eclipse': 10,
  'Malice Stone Pendant': 13,
  'Shackles of the Unscarred': 10,
  'Frostfire Bindings': 10,
  'Warlord\'s Silk Amice': 10,
  'Field Marshal\'s Silk Spaulders': 10,
  'Enigma Shoulderpads': 10
}

function parseAttribute(attribute: string, itemDataStr: string): number {
  // +11 Intellect-8 Stamina example
  const re = new RegExp('([\\+-])(\\d+) ' + attribute, 'm')
  const matches = itemDataStr.match(re)
  if (matches) {
    const sign = matches[1]
    const value = parseInt(matches[2], 10)
    if (sign === '+') {
      return value
    } else {
      return -value
    }
  }

  return 0
}

function parseArmor(itemDataStr: string): number {
  // UniqueNeck30 Armor example
  const matches = itemDataStr.match(/(\d+) Armor/m)
  if (matches) {
    const value = parseInt(matches[1], 10)
    return value
  }

  return 0
}

function parseDurability(itemDataStr: string): number {
  const matches = itemDataStr.match(/Durability (\d+) \/ (\d+)/m)
  if (matches) {
    const value = parseInt(matches[1], 10)
    return value
  }

  return 0
}

function parselevel(factsStr: string): number {
  const matches = factsStr.match(/Level: (\d+)/m)
  if (matches) {
    const value = parseInt(matches[1], 10)
    return value
  }

  return 0
}

function parseWeaponStats(itemDataStr: string): WeaponStats | undefined {
  const dmgMatches = itemDataStr.match(/([0-9.]+) - ([0-9.]+) (.*) Damage/m)
  if (!dmgMatches) {
    return undefined
  }
  const minDmg = parseFloat(dmgMatches[1])
  const maxDmg = parseFloat(dmgMatches[2])

  const speedMatches = itemDataStr.match(/Speed ([0-9.]+)/m)
  if (!speedMatches) {
    return undefined
  }
  const speed = parseFloat(speedMatches[1])

  const dpsMatches = itemDataStr.match(/([0-9.]+) damage per second/m)
  if (!dpsMatches) {
    return undefined
  }
  const dps = parseFloat(dpsMatches[1])

  return {
    minDmg: minDmg,
    maxDmg: maxDmg,
    speed: speed,
    dps: dps
  }
}

function parseEffects(effectStrs: Array<string>): Effects {
  // ['Equip: Increases damage and healing done by magical spells and effects by up to 12.',
  // 'Equip: Restores 4 mana per 5 sec.'] example

  let mp5 = 0
  let hp5 = 0
  let manaRegenWhileCasting = 0
  let healPower = 0
  let spellCrit = 0
  let spellHit = 0
  let spellPen = 0
  let spellHaste = 0
  let spellPower = 0
  let spellVamp = 0
  let frostSpellPower = 0
  let fireSpellPower = 0
  let arcaneSpellPower = 0
  let natureSpellPower = 0
  let shadowSpellPower = 0
  let targetTypes = 0
  let onUse = false
  const custom = []

  for (const effectStr of effectStrs) {
    // mp5
    const mp5Matches = effectStr.match(/(Equip|Set): Restores (\d+) mana (per|every) 5 sec./m)
    if (mp5Matches) {
      mp5 = parseInt(mp5Matches[2], 10)
      continue
    }

    // hp5
    const hp5Matches = effectStr.match(/(Equip|Set): Restores (\d+) health (per|every) 5 sec./m)
    if (hp5Matches) {
      hp5 = parseInt(hp5Matches[2], 10)
      continue
    }

    // mana while casting
    const manaRegenWhileCastingMatches = effectStr.match(
      /(Equip|Set): Allows (\d+)% of your Mana regeneration to continue while casting./m
    )
    if (manaRegenWhileCastingMatches) {
      manaRegenWhileCasting = parseInt(manaRegenWhileCastingMatches[2], 10)
      continue
    }

    // heal power
    const healPowerMatches = effectStr.match(
      /(Equip|Set): Increases healing done by spells and effects by up to (\d+)./m
    )
    if (healPowerMatches) {
      healPower = parseInt(healPowerMatches[2], 10)
      continue
    }

    // spell crit
    const spellCritMatches = effectStr.match(
      /(Equip|Set): Improves your chance to get a critical strike with spells by (\d+)%./m
    )
    if (spellCritMatches) {
      spellCrit = parseInt(spellCritMatches[2], 10)
      continue
    }

    const partySpellCritMatches = effectStr.match(
      /(Equip|Set): Increases the spell critical chance of all party members within (\d+) yards by (\d+)%./m
    )
    if (partySpellCritMatches) {
      spellCrit += parseInt(partySpellCritMatches[3], 10)
      continue
    }

    // spell hit
    const spellHitMatches = effectStr.match(/(Equip|Set): Improves your chance to hit with spells by (\d+)%./m)
    if (spellHitMatches) {
      spellHit = parseInt(spellHitMatches[2], 10)
      continue
    }

    // spell pen
    const spellPenMatches = effectStr.match(
      /(Equip|Set): Decreases the magical resistances of your spell targets by (\d+)./m
    )
    if (spellPenMatches) {
      spellPen = parseInt(spellPenMatches[2], 10)
      continue
    }

    // spell haste
    const spellHasteMatches = effectStr.match(/(Equip|Set): Increases .*casting speed by (\d+)%./m)
    if (spellHasteMatches) {
      spellHaste = parseInt(spellHasteMatches[2], 10)
      continue
    }

    // spell power
    const spellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage(.*)done by magical spells and effects by up to (\d+)./m
    )
    if (spellPowerMatches) {
      const healingStr = spellPowerMatches[2]
      spellPower = parseInt(spellPowerMatches[3], 10)

      if (healingStr.includes('healing')) {
        healPower = spellPower
      }
      continue
    }

    // target specific spell power
    const targetSpellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage done to (.*) by magical spells and effects by up to (\d+)\..*/m
    )
    if (targetSpellPowerMatches) {
      const targetTypesStr = targetSpellPowerMatches[2]
      if (targetTypesStr.includes('Undead')) {
        targetTypes |= TargetType.Undead
      }
      if (targetTypesStr.includes('Demon')) {
        targetTypes |= TargetType.Demon
      }
      if (targetTypesStr.includes('Elemental')) {
        targetTypes |= TargetType.Elemental
      }
      if (targetTypesStr.includes('Dragonkin')) {
        targetTypes |= TargetType.Dragonkin
      }
      if (targetTypesStr.includes('Giant')) {
        targetTypes |= TargetType.Giant
      }
      if (targetTypesStr.includes('Beast')) {
        targetTypes |= TargetType.Beast
      }
      if (targetTypesStr.includes('Mechanical')) {
        targetTypes |= TargetType.Mechanical
      }
      if (targetTypesStr.includes('Humanoid')) {
        targetTypes |= TargetType.Humanoid
      }

      spellPower = parseInt(targetSpellPowerMatches[3], 10)
      continue
    }

    // spell vamp TODO: this is a guess
    const spellVampMatches = effectStr.match(/(Equip|Set): (\d+)% of damage dealt is returned as healing./m)
    if (spellVampMatches) {
      spellVamp = parseInt(spellVampMatches[2], 10)
      continue
    }

    // frost spell power
    const frostSpellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage done by Frost spells and effects by up to (\d+)./m
    )
    if (frostSpellPowerMatches) {
      frostSpellPower = parseInt(frostSpellPowerMatches[2], 10)
      continue
    }

    // fire spell power
    const fireSpellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage done by Fire spells and effects by up to (\d+)./m
    )
    if (fireSpellPowerMatches) {
      fireSpellPower = parseInt(fireSpellPowerMatches[2], 10)
      continue
    }

    // nature spell power
    const natureSpellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage done by Nature spells and effects by up to (\d+)./m
    )
    if (natureSpellPowerMatches) {
      natureSpellPower = parseInt(natureSpellPowerMatches[2], 10)
      continue
    }

    // shadow spell power
    const shadowSpellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage done by Shadow spells and effects by up to (\d+)./m
    )
    if (shadowSpellPowerMatches) {
      shadowSpellPower = parseInt(shadowSpellPowerMatches[2], 10)
      continue
    }

    // arcane spell power
    const arcaneSpellPowerMatches = effectStr.match(
      /(Equip|Set): Increases damage done by Arcane spells and effects by up to (\d+)./m
    )
    if (arcaneSpellPowerMatches) {
      arcaneSpellPower = parseInt(arcaneSpellPowerMatches[2], 10)
      continue
    }

    if (effectStr.startsWith('Use:')) {
      onUse = true
    }

    // custom
    custom.push(effectStr)
  }

  return {
    mp5: mp5,
    hp5: hp5,
    manaRegenWhileCasting: manaRegenWhileCasting,
    healPower: healPower,
    spellCrit: spellCrit,
    spellHit: spellHit,
    spellPen: spellPen,
    spellHaste: spellHaste,
    spellPower: spellPower,
    spellVamp: spellVamp,
    frostSpellPower: frostSpellPower,
    fireSpellPower: fireSpellPower,
    arcaneSpellPower: arcaneSpellPower,
    natureSpellPower: natureSpellPower,
    shadowSpellPower: shadowSpellPower,
    targetTypes: targetTypes,
    onUse: onUse,
    custom: custom
  }
}

function parseItem(item: any): ItemJSON | undefined {
  try {
    const parentUrl = item['web-scraper-start-url']
    const url = item['item_links-href'] ? item['item_links-href'] : item['set_item_link-href']
    const id = parseInt(url.split('?item=').pop(), 10)
    let name = item['item_name']
    if (name.startsWith('Equip:')) {
      name = item['item_links']
    }

    let icon = item['icon'] !== '' ? item['icon'].match(/.*\/(.*)\.png/)[1] + '.jpg' : 'inv_misc_questionmark.jpg'
    if (icon === '.jpg') {
      icon = 'inv_misc_questionmark.jpg'
    }

    const armorType = item['armor_type']
    let slot = item['slot']

    const lowerSlot = slot.toLowerCase()
    // rename weapon slots
    if (lowerSlot === 'off hand' || lowerSlot === 'held in off-hand') {
      slot = ItemSlot.Offhand
    } else if (lowerSlot === 'one-hand') {
      slot = ItemSlot.Onehand
    } else if (lowerSlot === 'two-hand') {
      slot = ItemSlot.Twohand
    } else if (lowerSlot === 'main hand') {
      slot = ItemSlot.Mainhand
    }

    // check for missing icon
    if (!fs.existsSync('./public/wow-icons/' + icon)) {
      console.warn('missing icon for ' + 'https://database.turtle-wow.org/images/icons/large/' + icon)
    }

    // parse data
    const itemDataStr = item['data']
    const boe = itemDataStr.includes('Binds when equipped')
    const unique = itemDataStr.includes('Unique')

    const qualityCssClassNum = itemDataStr.match(/<b class="q(\d)">/m)[1]
    const quality = parseInt(qualityCssClassNum, 10)

    // armor
    const armor = parseArmor(itemDataStr)

    // durability
    const durability = parseDurability(itemDataStr)

    // weapon stats
    let weaponStats: WeaponStats | undefined
    if (
      slot === ItemSlot.Mainhand ||
      slot === ItemSlot.Onehand ||
      slot === ItemSlot.Twohand ||
      slot === ItemSlot.Ranged
    ) {
      weaponStats = parseWeaponStats(itemDataStr)
    }

    // attributes
    const strength = parseAttribute('Strength', itemDataStr)
    const agility = parseAttribute('Agility', itemDataStr)
    const stamina = parseAttribute('Stamina', itemDataStr)
    const intellect = parseAttribute('Intellect', itemDataStr)
    const spirit = parseAttribute('Spirit', itemDataStr)

    // resistances
    const fireRes = parseAttribute('Fire Resistance', itemDataStr)
    const natureRes = parseAttribute('Nature Resistance', itemDataStr)
    const frostRes = parseAttribute('Frost Resistance', itemDataStr)
    const shadowRes = parseAttribute('Shadow Resistance', itemDataStr)
    const arcaneRes = parseAttribute('Arcane Resistance', itemDataStr)

    const effectsJSON = JSON.parse(item['effects']).map((obj: any) => obj.effects)
    const effects = parseEffects(effectsJSON)

    // spell pen corrections
    if (spellPenCorrections[name]) {
      effects.spellPen = spellPenCorrections[name]
    }

    const setName = item['set_name']
    const setBonusesText = item['set_bonus'].split('\n')

    // loop through set bonuses text
    const setBonuses: Record<number, Effects> = {}
    try {
      for (const setBonusText of setBonusesText) {
        if (setBonusText === '') {
          continue
        }
        // parse out amount which looks like (2) Set:
        // and effect
        const parsedSetBonusMatches = setBonusText.match(/\((\d+)\) (Set: .*)/m)
        const minNumItemsForBonus = parseInt(parsedSetBonusMatches[1], 10)
        const effectText = parsedSetBonusMatches[2]
        const effects = parseEffects([effectText])

        // arcanist regalia correction
        if (setName === 'Arcanist Regalia' && minNumItemsForBonus === 5) {
          effects.spellPen = ArcanistRegalia5SetSpellPenCorrection
        }

        if (!setBonuses[minNumItemsForBonus]) {
          setBonuses[minNumItemsForBonus] = effects
        } else {
          // combine effects
          setBonuses[minNumItemsForBonus] = {
            mp5: setBonuses[minNumItemsForBonus].mp5 + effects.mp5,
            hp5: setBonuses[minNumItemsForBonus].hp5 + effects.hp5,
            healPower: setBonuses[minNumItemsForBonus].healPower + effects.healPower,
            spellCrit: setBonuses[minNumItemsForBonus].spellCrit + effects.spellCrit,
            spellHit: setBonuses[minNumItemsForBonus].spellHit + effects.spellHit,
            spellPen: setBonuses[minNumItemsForBonus].spellPen + effects.spellPen,
            spellHaste: setBonuses[minNumItemsForBonus].spellHaste + effects.spellHaste,
            spellPower: setBonuses[minNumItemsForBonus].spellPower + effects.spellPower,
            spellVamp: setBonuses[minNumItemsForBonus].spellVamp + effects.spellVamp,
            frostSpellPower: setBonuses[minNumItemsForBonus].frostSpellPower + effects.frostSpellPower,
            fireSpellPower: setBonuses[minNumItemsForBonus].fireSpellPower + effects.fireSpellPower,
            arcaneSpellPower: setBonuses[minNumItemsForBonus].arcaneSpellPower + effects.arcaneSpellPower,
            natureSpellPower: setBonuses[minNumItemsForBonus].natureSpellPower + effects.natureSpellPower,
            shadowSpellPower: setBonuses[minNumItemsForBonus].shadowSpellPower + effects.shadowSpellPower,
            targetTypes: setBonuses[minNumItemsForBonus].targetTypes | effects.targetTypes, // TODO warning not gonna work perfectly if only one has target types
            onUse: false,
            custom: setBonuses[minNumItemsForBonus].custom.concat(effects.custom)
          }
        }
      }
    } catch (e) {
      console.log(e)
      console.warn('Could not parse set bonus: |' + setBonusesText + '| for item: ' + name)
    }

    let setItems = []
    setItems = JSON.parse(item['set_items']).map((obj: any) => obj.set_items)

    let obtainType = ObtainType.Other
    let seeAlsoNum = 0

    // Sold by (4) example
    if (item['see_also_type'] === 'Fished in') {
      obtainType = ObtainType.FishedIn
    } else if (item['see_also_type'] !== '') {
      const seeAlsoType = item['see_also_type'].match(/([\w ]+) \((\d+)\)/m)
      const typeStr = seeAlsoType[1]
      switch (typeStr) {
        case 'Sold by': {
          obtainType = ObtainType.Vendor
          break
        }
        case 'Dropped by': {
          obtainType = ObtainType.Drop
          break
        }
        case 'Reward from': {
          obtainType = ObtainType.Quest
          break
        }
        case 'Created by': {
          obtainType = ObtainType.Crafted
          break
        }
        case 'Contained in': {
          obtainType = ObtainType.ContainedIn
          break
        }
        case 'Reagent for': {
          obtainType = ObtainType.ReagentFor
          break
        }
        case 'Fished in': {
          obtainType = ObtainType.FishedIn
          break
        }
        case 'Objective of': {
          obtainType = ObtainType.ObjectiveOf
          break
        }
        default: {
          console.warn('Unknown obtain type: ' + typeStr + ' for item: ' + name)
          obtainType = ObtainType.Other
        }
      }

      seeAlsoNum = parseInt(seeAlsoType[2], 10)
    }

    let location = ''
    let mob = ''
    let pvp = false
    let dropChance = undefined

    // check for field marshal and raiment as they don't have see also data for some reason
    if (
      name.includes('Field Marshal\'s') ||
      name.includes('Warlord\'s') ||
      name.includes('Blood Guard\'s') ||
      name.includes('Champion\'s') ||
      name.includes('Legionnaire\'s') ||
      name.includes('Knight-Lieutenant\'s') ||
      name.includes('Knight-Captain\'s') ||
      name.includes('Lieutenant Commander\'s') ||
      name.includes('Commander\'s') ||
      name.includes('Marshal\'s') ||
      name.includes('General\'s') ||
      name.includes('Warlord\'s') ||
      name.includes('High Warlord\'s') ||
      name.includes('Grand Marshal\'s') ||
      name.includes('Bloody Gladiator\'s')
    ) {
      pvp = true
    }

    if (item['see_also_1'] !== '' && item['see_also_1'] !== '[]') {
      const seeAlso1 = JSON.parse(item['see_also_1'])[0]['see_also_1']

      switch (obtainType) {
        case ObtainType.Vendor: {
          const seeAlso3 = JSON.parse(item['see_also_3'])[0]['see_also_3']
          mob = seeAlso1
          location = seeAlso3
          if (
            location === PVPLocations.AlteracValley ||
            location === PVPLocations.ArathiBasin ||
            location === PVPLocations.WarsongGulch ||
            mob.includes('Supply Officer')
          ) {
            pvp = true
          }
          break
        }
        case ObtainType.Drop: {
          const seeAlso3 = JSON.parse(item['see_also_3'])[0]['see_also_3']
          const seeAlso6 = JSON.parse(item['see_also_6'])[0]['see_also_6']
          mob = `Dropped by: ${seeAlso1}`
          location = seeAlso3
          dropChance = parseFloat(seeAlso6)
          break
        }
        case ObtainType.Quest: {
          const seeAlso6 = JSON.parse(item['see_also_6'])[0]['see_also_6']
          mob = `Quest: ${seeAlso1}`
          location = seeAlso6
          break
        }
        case ObtainType.ObjectiveOf: {
          const seeAlso6 = JSON.parse(item['see_also_6'])[0]['see_also_6']
          mob = `Objective of: ${seeAlso1}`
          location = seeAlso6
          if (
            location === PVPLocations.AlteracValley ||
            location === PVPLocations.ArathiBasin ||
            location === PVPLocations.WarsongGulch
          ) {
            pvp = true
          }
          break
        }
        case ObtainType.Crafted: {
          const seeAlso2 = JSON.parse(item['see_also_2'])[0]['see_also_2']
          mob = `Crafted by: ${seeAlso2}`
          break
        }
        case ObtainType.ReagentFor: {
          const seeAlso2 = JSON.parse(item['see_also_2'])[0]['see_also_2']
          mob = `Reagent for: ${seeAlso2}`
          break
        }
        case ObtainType.ContainedIn: {
          mob = `Contained in: ${seeAlso1}`
          break
        }
        case ObtainType.FishedIn: {
          location = `Fished in: ${seeAlso1}`
          break
        }
      }

      if (location === 'undefined') {
        location = ''
      }

      // check for world bosses
      Object.values(WorldBoss).forEach(boss => {
        if (mob.includes(boss)) {
          obtainType = ObtainType.WorldBoss
        }
      })
    }

    const matches = item.facts.match(allowableClassesRegex)
    const allowableClasses = matches[1].split(', ')

    // item level
    const level = parselevel(item.facts)

    return {
      id,
      name,
      url,
      parentUrl,
      level,
      icon,
      quality,
      armorType,
      slot,
      boe,
      unique,
      armor,
      durability,
      weaponStats,
      strength,
      agility,
      stamina,
      intellect,
      spirit,
      fireRes,
      natureRes,
      frostRes,
      shadowRes,
      arcaneRes,
      effects,
      setName,
      setBonuses,
      setItems,

      location,
      obtainType,
      obtainFrom: mob,
      seeAlsoNum,
      pvp,
      dropChance,

      allowableClasses
    }
  } catch (e) {
    console.error(e)
    console.warn(item)
    console.warn('Could not parse item: ' + item.item_name)
  }
}

const itemCSVs = [
  './src/wow/db/customItems.csv',
  './src/wow/db/customItemSets.csv',
  './src/wow/db/items.csv',
  './src/wow/db/items2.csv',
  './src/wow/db/itemsets.csv',
  './src/wow/db/itemsets2.csv',
  './src/wow/db/rings.csv',
  './src/wow/db/olditems.csv'
]

const start = async function() {
  // create csvarray from itemCSVs
  let csvArray: any[] = []
  for (const itemCSVPath of itemCSVs) {
    console.warn('Parsing CSV: ' + itemCSVPath)
    csvArray = csvArray.concat(await csv().fromFile(itemCSVPath))
  }

  const parsedItems: Array<ItemJSON> = []
  const parsedItemIds: Record<number, boolean> = {} // used to remove duplicates due to itemsets

  // loop through csv rows
  for (const item of csvArray) {
    // ignore grey items that didn't get parsed properly due to missing 'Binds'
    if (item.data === '[]' || item.data === '') {
      continue
    }
    // check if usable by warlock
    // regex to search for  Allowable Classes:
    const matches = item.facts.match(allowableClassesRegex)
    if (!matches) {
      console.warn('No allowable classes found for item: ' + item.item_name)
      continue
    }

    let allowableClasses = matches[1].split(', ')
    if (item.item_name.startsWith('Sorcerer\'s')) {
      allowableClasses = ['Mage']
    }

    if (allowableClasses.includes('Warlock') || allowableClasses.includes('All')) {
      // continue parsing
      const parsedItem = parseItem(item)
      if (parsedItem && !parsedItemIds[parsedItem.id]) {
        // only include warlock atiesh
        if (parsedItem.name === 'Atiesh, Greatstaff of the Guardian' && parsedItem.id !== 22630) {
          console.warn('Ignoring atiesh for other class id: ' + parsedItem.id)
        } else if (parsedItem.id === 84036 || parsedItem.id === 84001 || parsedItem.id === 84003) {
          console.warn('Ignoring Rose of Sanguine Rage | Loop of acceleration | Signet of the Broken Oath')
        } else {
          parsedItems.push(parsedItem)
          parsedItemIds[parsedItem.id] = true
        }
      } else if (parsedItem && parsedItemIds[parsedItem.id]) {
        // console.warn('Duplicate item id: ' + parsedItem.id + ' for item: ' + parsedItem.name)
      }
    }
  }
  fs.writeFile(outputPath, JSON.stringify(parsedItems, null, 2), 'utf8', function(err: any) {
    if (err) throw err
  })
}

start()
  .then(() => {
    console.warn('Done Importing')
  })
  .catch(err => console.error(err))
