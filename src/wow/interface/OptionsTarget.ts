import MagicSchool from '../enum/MagicSchool'
import TargetType from '../enum/TargetType'

export default interface OptionsTarget {
  level: number
  shimmer: MagicSchool
  type: TargetType
  spellResistance: number
  debuffs: Record<string, number>
}
