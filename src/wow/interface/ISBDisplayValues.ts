export default interface ISBDisplayValues {
  desc: string
  numProcs: number
  castsISBActive: number
  uptimePercentage: number
  addedDotDmg: number
  addedSBDmg: number
  totalDmg: number
  encounterDPS: number
}
