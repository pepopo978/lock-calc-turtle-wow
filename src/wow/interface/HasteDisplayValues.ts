export default interface HasteDisplayValues {
  hastePercent: number
  castTimeReduction: number
  additionalCasts: number
  partialCastsWithoutHaste: number
  partialCastsWithHaste: number
  totalDmg: number
  avgSPPerCast: number
  encounterDPS: number
}
