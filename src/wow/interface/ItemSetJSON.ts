import ItemJSON from '../interface/ItemJSON'

export default interface ItemSetJSON {
  name: string
  tailoring: boolean
  spellHit?: number
  spellCrit?: number
  spellDamage?: number
  names: string[]
  items?: ItemJSON[]
  score?: number
}
