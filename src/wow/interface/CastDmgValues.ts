export default interface CastDmgValues {
  min: number
  max: number
  avg: number
  text: string
  dps?: number
}
