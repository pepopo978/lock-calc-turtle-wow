import LockSpellCounter from '@/wow/interface/LockSpellCounter'

export default interface CastData {
  casts: LockSpellCounter
  ticks: LockSpellCounter
}
