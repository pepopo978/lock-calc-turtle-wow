import TargetType from '../enum/TargetType'
import MagicSchool from '../enum/MagicSchool'

export default interface TrinketInfo {
  encLen: number
  magicSchool: MagicSchool
  targetType: TargetType
  castDmgEV: number
  castLag: number
  spellHaste: number
  spellBaseCastTime: number
  spellEffectiveCastTime: number
  spellPower: number
  spellCoefficient: number
  spellPowerToDmgRatio: number
}
