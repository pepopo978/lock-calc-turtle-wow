import CastPeriodicDmgValues from './CastPeriodicDmgValues'

export default interface CastDmgObject {
  base: CastPeriodicDmgValues
  actual: CastPeriodicDmgValues
  effective: CastPeriodicDmgValues
}
