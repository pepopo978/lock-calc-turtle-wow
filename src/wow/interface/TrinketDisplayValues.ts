export default interface TrinketDisplayValues {
  name: string
  type: 'passive' | 'on use'
  avgSPPerBuffedCast?: number
  buffedCasts?: number
  uptimeDPS?: number
  avgSPPerCast: number
  encounterDPS: number
  totalCasts: number
  totalDmg: number
  totalSP: number
  uptime: number
}
