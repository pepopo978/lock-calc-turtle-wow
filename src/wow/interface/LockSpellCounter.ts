export default interface LockSpellCounter {
  shadowBolt: number
  immolate: number
  corruption: number
  curseOfAgony: number
  curseOfShadow: number
}
