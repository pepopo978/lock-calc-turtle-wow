export default interface CastDmgValues {
  tick: number
  total: number
  uptime: number
  tickText: string
  totalText: string
}
