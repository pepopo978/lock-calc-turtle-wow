export default interface TrinketDmgValues {
  name: string
  passive?: {
    avgSPPerCast: number
    encounterDPS: number
    totalCasts: number
    totalDmg: number
    totalSP: number
    uptime: number
  }
  onUse?: {
    avgSPPerBuffedCast: number
    avgSPPerCast: number
    buffedCasts: number
    encounterDPS: number
    totalCasts: number
    totalDmg: number
    totalSP: number
    uptime: number
    uptimeDPS: number
  }
}
