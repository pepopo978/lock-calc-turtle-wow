export default interface GearButtonBarOptions {
  showFillItems: boolean
  showLock: boolean
  showUnlock: boolean
  showUnequip: boolean
  showShare: boolean
  showImport: boolean
}
