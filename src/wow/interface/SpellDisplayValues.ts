export default interface SpellDisplayValues {
  name: string
  effectiveCastTime: number
  effectiveTickDmg: number
  totalCasts: number
  totalTicks: number
  totalDotDmg: number
  totalDirectDmg: number
  totalDmg: number
  encounterDPS: number
}
