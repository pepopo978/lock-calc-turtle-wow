import TargetType from '../enum/TargetType'
import MagicSchool from '../enum/MagicSchool'
import SortOrder from '../enum/SortOrder'
import ItemSlot from '../enum/ItemSlot'
import { ObtainType } from './ItemJSON'

export default interface ScoreInfo {
  encLen: number
  maxItemLevel?: number
  itemType?: ObtainType
  pvpItems: boolean
  partialHasteCasts: boolean
  magicSchool: MagicSchool
  targetType: TargetType
  castDmgEV: number
  castLag: number
  effectiveDmgMultiplier: number
  spellName: string
  spellPower: number
  spellCoefficient: number
  spellPowerToDmgRatio: number
  spellHitWeight: number
  spellCritWeight: number
  spellPenetrationWeight: number
  spellHaste: number
  spellBaseCastTime: number
  spellEffectiveCastTime: number
  spellCastTimeWithoutHaste: number
  spellCrit: number
  slot: ItemSlot
  sortOrder?: SortOrder
}
