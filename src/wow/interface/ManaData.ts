export default interface ManaData {
  manaPotionsUsed: number
  manaPotionsRestored: number
  teasUsed: number
  teasRestored: number
  finalMana: number
  wentOOM: boolean
}
