export default interface WeaponStats {
  minDmg: number
  maxDmg: number
  speed: number
  dps: number
}
