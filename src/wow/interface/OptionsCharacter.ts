import Gender from '../enum/Gender'
import PlayableRace from '../enum/PlayableRace'
import PlayableClass from '../enum/PlayableClass'
import Buffs from '../enum/Buffs'

export default interface OptionsCharacter {
  level: number
  gender: Gender
  race: PlayableRace
  class: PlayableClass
  buffs: Buffs[]
  talents: {
    affliction: {
      suppression: number
      improvedCorruption: number

      improvedCurseOfWeakness: number
      improvedDrainSoul: number
      improvedLifeTap: number
      soulSiphon: number

      improvedCurseOfAgony: number
      felConcentration: number
      amplifyCurse: number

      grimReach: number
      nightfall: number

      siphonLife: number
      curseOfExhaustion: number
      improvedCurseOfExhaustion: number

      shadowMastery: number

      darkPact: number
    }
    demonology: {
      masterConjurer: number
      improvedImp: number
      demonicEmbrace: number

      improvedHealthFunnel: number
      improvedVoidwalker: number
      felIntellect: number

      improvedSuccubus: number
      felDomination: number
      felStamina: number
      demonicAegis: number

      masterSummoner: number
      unholyPower: number

      improvedEnslaveDemon: number
      demonicSacrifice: number

      masterDemonologist: number

      soulLink: number
    }
    destruction: {
      improvedShadowBolt: number
      cataclysm: number

      bane: number
      aftermath: number

      improvedFirebolt: number
      improvedLashOfPain: number
      devastation: number
      shadowburn: number

      intensity: number

      destructiveReach: number
      improvedSearingPain: number

      pyroclasm: number
      improvedImmolate: number
      ruin: number

      emberstorm: number

      conflagrate: number
    }
  }
}
