import Item from '../class/Item'

export default interface ItemSetData {
  virtualItems: Item[] | undefined
}
