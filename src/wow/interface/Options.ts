import OptionsCharacter from './OptionsCharacter'
import OptionsTarget from './OptionsTarget'
import ItemSlot from '../enum/ItemSlot'
import { ObtainType } from './ItemJSON'

export default interface Options {
  pvpItems: boolean
  partialCasts: boolean
  partialTicks: boolean
  partialHasteCasts: boolean
  encLen: number
  maxItemlevel: number | undefined
  itemType: ObtainType | undefined
  spellName: string
  spec: string
  itemSearchSlot?: ItemSlot
  enchantSearchSlot?: ItemSlot
  castLag: number
  character: OptionsCharacter
  target: OptionsTarget
  cos: boolean
  cor: boolean
  coa: boolean
  imm: boolean
  imp: boolean
  useLt: boolean
  useMp: boolean
  useQp: boolean
  useTea: boolean
  useEvo: boolean
  numVates: number
}
