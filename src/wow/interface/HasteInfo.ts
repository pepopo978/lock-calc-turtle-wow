export default interface HasteInfo {
  encLen: number
  partialHasteCasts: boolean
  castDmgEV: number
  spellPowerToDmgRatio: number
  spellHaste: number
  spellBaseCastTime: number
  spellEffectiveCastTime: number
  spellCastTimeWithoutHaste: number
  castLag: number
  spellName: string
}
