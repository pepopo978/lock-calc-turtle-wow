import items from './db/lockItems.json'
import oldItems from './db/lockItemsOld.json'

const newItemsMap: Record<string, string> = {}
const oldItemsMap: Record<string, string> = {}
// populate itemsmap from items with item name and url
items.forEach((item: any) => {
  newItemsMap[item.name] = item.id + ' ' + item.url
})

// populate oldItemsMap from oldItems with item name and url
oldItems.forEach((item: any) => {
  oldItemsMap[item.name] = item.id + ' ' + item.url
})

// loop through old set
// if new set does not have item, add to diff
console.log('missing')
Object.keys(oldItemsMap).forEach(key => {
  if (!newItemsMap[key]) {
    console.log(oldItemsMap[key], key)
  }
})

console.log('added')
Object.keys(newItemsMap).forEach(key => {
  if (!oldItemsMap[key]) {
    console.log(newItemsMap[key], key)
  }
})
