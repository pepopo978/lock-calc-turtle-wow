import ItemJSON from '../interface/ItemJSON'
import TrinketDmgValues from '../interface/TrinketDmgValues'
import TrinketInfo from '../interface/TrinketInfo'
import { GLOBAL_COOLDOWN } from '@/wow/module/Constants'

export function getTrinketEffectiveDuration(
  effectiveDuration: number, // haste trinkets will effect final cast that goes past the duration, include that cast in the effective duration
  trinketCooldown: number,
  encLen: number
): number {
  if (effectiveDuration >= encLen) {
    return encLen
  } else if (trinketCooldown >= encLen) {
    // single usage
    return effectiveDuration
  } else {
    const numCooldownsUsed = Math.floor(encLen / trinketCooldown) + 1

    let totalDuration = effectiveDuration * (numCooldownsUsed - 1) // start with all the full durations

    // check if last usage is full duration or partial duration
    const lastUsageStartTime = (numCooldownsUsed - 1) * trinketCooldown
    const lastUsageFinishTime = lastUsageStartTime + effectiveDuration
    if (encLen > lastUsageFinishTime) {
      // full duration
      totalDuration += effectiveDuration
    } else {
      // partial duration
      totalDuration += encLen - lastUsageStartTime
    }

    return totalDuration
  }
}

function getTrinketPassiveDmgValues(
  itemJSON: ItemJSON,
  trinketInfo: TrinketInfo
): {
  avgSPPerCast: number
  encounterDPS: number
  totalCasts: number
  totalDmg: number
  totalSP: number
  uptime: number
} {
  {
    const totalCasts = Math.floor(trinketInfo.encLen / trinketInfo.spellEffectiveCastTime)
    let passiveSPPerCast = 0
    if (itemJSON.score) {
      passiveSPPerCast = itemJSON.score
      // score includes onUseScore, so subtract it out if it exists
      if (itemJSON.onUseScore) {
        passiveSPPerCast -= itemJSON.onUseScore
      }
    }
    const totalDmg = passiveSPPerCast * totalCasts * trinketInfo.spellPowerToDmgRatio

    return {
      totalCasts: totalCasts,
      uptime: trinketInfo.encLen,
      avgSPPerCast: passiveSPPerCast,
      totalDmg: Math.round(totalDmg),
      totalSP: passiveSPPerCast * totalCasts,
      encounterDPS: Math.round((totalDmg / trinketInfo.encLen) * 10) / 10
    }
  }
}

export function trinketDmgValues(itemJSON: ItemJSON, trinketInfo: TrinketInfo): TrinketDmgValues {
  if (itemJSON.name === 'Mind Quickening Gem') {
    return mindQuickeningTrinketValues(itemJSON, 20, 300, trinketInfo)
  }
  if (itemJSON.name === 'Shard of the Fallen Star') {
    return shardOfTheFallenStarTrinketValues(itemJSON, 20, 150, trinketInfo, 0)
  }

  switch (itemJSON.name) {
    case 'Draconic Infused Emblem':
      return onUseTrinketDMGValues(itemJSON, 100, 15, 75, 0, trinketInfo)
    case 'Talisman of Ephemeral Power':
      return onUseTrinketDMGValues(itemJSON, 172, 15, 90, 0, trinketInfo)
    case 'Zandalarian Hero Charm':
      return onUseTrinketDMGValues(itemJSON, 204, 20, 120, 17, trinketInfo)
    case 'The Restrained Essence of Sapphiron': {
      const values = onUseTrinketDMGValues(itemJSON, 130, 20, 120, 0, trinketInfo)
      values.passive = getTrinketPassiveDmgValues(itemJSON, trinketInfo) // add in passive effect
      return values
    }
    case 'Eye of Moam':
      return onUseTrinketDMGValues(itemJSON, 50, 30, 180, 0, trinketInfo)
    case 'Hazza\'rah\'s Charm of Magic': // TODO (complicated cuz it buffs crit chance and crit dmg)
    default:
      return {
        name: itemJSON.name,
        passive: getTrinketPassiveDmgValues(itemJSON, trinketInfo)
      }
  }
}

function onUseTrinketDMGValues(
  itemJSON: ItemJSON,
  trinketBonus: number,
  trinketDuration: number,
  trinketCooldown: number,
  trinketReductionPerCast: number,
  trinketInfo: TrinketInfo
): TrinketDmgValues {
  const uptime = getTrinketEffectiveDuration(trinketDuration, trinketCooldown, trinketInfo.encLen)
  const buffedCastsForOneFullDuration = Math.floor(trinketDuration / trinketInfo.spellEffectiveCastTime)

  let buffedCasts = buffedCastsForOneFullDuration * Math.floor(uptime / trinketDuration)
  // add in partial duration
  buffedCasts += Math.floor((uptime % trinketDuration) / trinketInfo.spellEffectiveCastTime)

  const totalCasts = Math.floor(trinketInfo.encLen / trinketInfo.spellEffectiveCastTime)
  let totalSP = trinketBonus * buffedCasts
  if (trinketReductionPerCast) {
    totalSP = 0
    const buffedCastsForSingleCooldown = Math.floor(trinketDuration / trinketInfo.spellEffectiveCastTime)

    for (let i = 0; i < buffedCasts; i++) {
      const reduction = trinketReductionPerCast * (i % buffedCastsForSingleCooldown)
      const bonus = trinketBonus - reduction
      totalSP += bonus
    }
  }
  const avgSPPerBuffedCast = totalSP / buffedCasts
  const avgSPPerCast = totalSP / totalCasts

  const totalDmg = Math.round(trinketInfo.spellPowerToDmgRatio * totalSP) // rough estimation
  const uptimeDPS = totalDmg / uptime
  const encounterDPS = totalDmg / trinketInfo.encLen

  return {
    name: itemJSON.name,
    onUse: {
      buffedCasts,
      totalCasts,
      uptime,
      avgSPPerBuffedCast,
      avgSPPerCast,
      totalDmg,
      totalSP,
      uptimeDPS,
      encounterDPS
    }
  }
}

function mindQuickeningTrinketValues(
  itemJSON: ItemJSON,
  trinketDuration: number,
  trinketCooldown: number,
  trinketInfo: TrinketInfo
): TrinketDmgValues {
  const totalHasteScalingFactor = (1 + 33 / 100) * (1 + trinketInfo.spellHaste / 100)

  const baseCastTimeWithoutLag = trinketInfo.spellBaseCastTime - trinketInfo.castLag
  const castTimeWhileActive =
    Math.max(baseCastTimeWithoutLag / totalHasteScalingFactor, GLOBAL_COOLDOWN) + trinketInfo.castLag

  const numCastsStarted = Math.ceil(trinketDuration / castTimeWhileActive)
  const effectiveSingleDuration = numCastsStarted * castTimeWhileActive

  const uptime = getTrinketEffectiveDuration(effectiveSingleDuration, trinketCooldown, trinketInfo.encLen)

  let buffedCasts = uptime / castTimeWhileActive
  let normalCasts = uptime / trinketInfo.spellEffectiveCastTime

  // calculate SP equivalents in terms of the original total amount of casts to enable comparison to other trinkets
  const totalCastsWithoutTrinket = Math.floor(trinketInfo.encLen / trinketInfo.spellEffectiveCastTime)
  const totalCastsWithTrinket =
    buffedCasts + Math.floor((trinketInfo.encLen - uptime) / trinketInfo.spellEffectiveCastTime)

  const increaseInCasts = buffedCasts - normalCasts

  // calculate damage added by the trinket
  const totalDmg = Math.round(increaseInCasts * trinketInfo.castDmgEV)
  const dmgPerCast = totalDmg / totalCastsWithoutTrinket
  const avgSPPerCast = dmgPerCast / trinketInfo.spellPowerToDmgRatio
  const totalSP = avgSPPerCast * totalCastsWithoutTrinket

  const avgSPPerBuffedCast = (dmgPerCast * increaseInCasts) / trinketInfo.spellPowerToDmgRatio
  const uptimeDPS = totalDmg / uptime
  const encounterDPS = totalDmg / trinketInfo.encLen

  return {
    name: itemJSON.name,
    onUse: {
      buffedCasts: Math.round(increaseInCasts * 10) / 10,
      totalCasts: Math.round(totalCastsWithTrinket * 10) / 10, // display total buffed casts despite using totalCasts for calculations
      uptime: Math.round(uptime * 10) / 10,
      avgSPPerBuffedCast,
      avgSPPerCast,
      totalDmg,
      totalSP,
      uptimeDPS,
      encounterDPS
    }
  }
}

function shardOfTheFallenStarTrinketValues(
  itemJSON: ItemJSON,
  _trinketDuration: number,
  trinketCooldown: number,
  trinketInfo: TrinketInfo,
  _expectedDmgPerCast: number
): TrinketDmgValues {
  const uptime = 0

  const usages = Math.floor(trinketInfo.encLen / trinketCooldown) + 1

  const totalCasts = Math.floor(trinketInfo.encLen / trinketInfo.spellEffectiveCastTime)

  const avgCastDmg = 421
  const coefficient = 0.7 // estimation

  const totalDmg = Math.round(
    usages * (avgCastDmg + coefficient * trinketInfo.spellPower * trinketInfo.spellPowerToDmgRatio)
  )
  const dmgPerCast = totalDmg / totalCasts

  const avgSPPerBuffedCast = totalDmg / usages / trinketInfo.spellPowerToDmgRatio
  const avgSPPerCast = dmgPerCast / trinketInfo.spellPowerToDmgRatio
  const totalSP = 0
  const uptimeDPS = 0
  const encounterDPS = totalDmg / trinketInfo.encLen

  return {
    name: itemJSON.name,
    passive: getTrinketPassiveDmgValues(itemJSON, trinketInfo),
    onUse: {
      buffedCasts: usages,
      totalCasts: totalCasts,
      uptime,
      avgSPPerBuffedCast,
      avgSPPerCast,
      totalDmg,
      totalSP,
      uptimeDPS,
      encounterDPS
    }
  }
}
