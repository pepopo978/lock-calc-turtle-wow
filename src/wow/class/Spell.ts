import MagicSchool from '../enum/MagicSchool'
import SpellCoefficient from '../interface/SpellCoefficient'
import SpellJSON from '../interface/SpellJSON'
import { GLOBAL_COOLDOWN } from '../module/Constants'
import Query from '../module/Query'
import TalentSchool from '@/wow/enum/TalentSchool'

/**
 * Spell details. These are base values that don't factor in talents, spellpower, buffs, debuffs, etc.
 */
export default class Spell {
  name: string
  spellJSON: SpellJSON

  constructor(name: string) {
    this.name = name
    const spell = Query.Spell({ name: name })
    if (spell === undefined) {
      throw new Error(`Spell ${name} not found`)
    }
    this.spellJSON = spell
  }

  /**
   * Return base (short) name, parsed from name.
   */
  get baseName(): string {
    if (!this.spellJSON) {
      return 'None'
    }
    const splitStr = this.spellJSON.name.split(' ')
    splitStr.length = splitStr.length - 2
    return splitStr.join(' ')
  }

  /**
   * Return spell rank, parsed from name.
   */
  get rank(): string {
    if (!this.spellJSON) {
      return '0'
    }
    const splitStr = this.spellJSON.name.split(' ')
    return splitStr[splitStr.length - 1]
  }

  get icon(): string {
    if (!this.spellJSON) {
      return 'classic_temp.jpg'
    }
    return `${this.spellJSON.icon}.jpg`
  }

  get iconFullPath(): string {
    return process.env.BASE_URL + 'wow-icons/' + this.icon
  }

  /**
   * Return spell type (direct, periodic or hybrid)
   */
  get type(): string {
    if (!this.spellJSON) {
      return 'direct'
    }
    return this.spellJSON.type.toUpperCase()
  }

  get canPartialResist(): boolean {
    // todo add checks for other spells
    return true
  }

  /**
   * Return spell reqLvl, unmodified.
   */
  get reqLvl(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.reqLvl
  }

  /**
   * Return cast time, limited to globalCoolDown FIXME: dont limit to gcd here
   */
  get castTime(): number {
    if (!this.spellJSON) {
      return GLOBAL_COOLDOWN
    }
    return Math.max(GLOBAL_COOLDOWN, this.spellJSON.castTime)
  }

  /**
   * Return spell magicSchool, unmodified.
   */
  get magicSchool(): MagicSchool {
    if (!this.spellJSON) {
      return MagicSchool.Holy
    }
    return this.spellJSON.magicSchool
  }

  get talentSchool(): TalentSchool {
    // todo add checks for other spells
    return TalentSchool.Destruction
  }

  get magicSchoolText(): string {
    return MagicSchool[this.magicSchool]
  }

  get isNature(): boolean {
    return this.magicSchool === MagicSchool.Nature
  }

  get isShadow(): boolean {
    return this.magicSchool === MagicSchool.Shadow
  }

  get isArcane(): boolean {
    return this.magicSchool === MagicSchool.Arcane
  }

  get isFire(): boolean {
    return this.magicSchool === MagicSchool.Fire
  }

  get isFrost(): boolean {
    return this.magicSchool === MagicSchool.Frost
  }

  /**
   * Return spell range, unmodified.
   */
  get range(): number {
    if (!this.spellJSON) {
      return 30
    }
    return this.spellJSON.range
  }

  /**
   * Return mana cost, unmodified.
   */
  get manaCost(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.manaCost
  }

  /**
   * Return spell minimum damage, unmodified.
   */
  get minDmg(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.minDmg ? this.spellJSON.minDmg : 0
  }

  /**
   * Return spell max damage, unmodified.
   */
  get maxDmg(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.maxDmg ? this.spellJSON.maxDmg : 0
  }

  /**
   * avg spell damage (minDmg + maxDmg) / 2.
   */
  get avgDmg(): number {
    return ((this.minDmg ? this.minDmg : 0) + (this.maxDmg ? this.maxDmg : 0)) / 2
  }

  get tickDmg(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.tickDmg ? this.spellJSON.tickDmg : 0
  }

  get tickRate(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.tickRate ? this.spellJSON.tickRate : 0
  }

  get ticks(): number {
    return this.duration > 0 && this.tickRate > 0 ? this.duration / this.tickRate : 0
  }

  get duration(): number {
    if (!this.spellJSON) {
      return 0
    }
    return this.spellJSON.duration ? this.spellJSON.duration : 0
  }

  get uptime(): number {
    const repeatCastTime = Math.max(this.castTime, GLOBAL_COOLDOWN)
    if (this.duration > repeatCastTime) {
      return 100
    }
    return (100 * this.duration) / repeatCastTime
  }

  get periodicDmg(): number {
    return this.tickDmg * (this.duration / this.tickRate)
  }

  get secondaryEffect(): string | undefined {
    if (!this.spellJSON) {
      return ''
    }
    return this.spellJSON.secondary
  }

  get isBinary(): boolean {
    return this.secondaryEffect && this.secondaryEffect !== '' ? true : false
  }

  /**
   * Return spell coefficients. There are three types of spells, each with their
   * own coefficient formulas: direct, periodic, and hybrid (direct + periodic). Spells also
   * suffer penalties when they're below level 20 or have secondary spell
   * effects (e.g. insect swarm)
   *
   * Source: https://classicwow.live/guides/670/ozgar-s-downranking-guide-tool
   */
  get coefficient(): SpellCoefficient {
    return {
      direct: this.spellJSON.directDmgCoefficient,
      periodic: this.spellJSON.periodicDmgCoefficient
    }
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          try {
            const val = (this as any)[key]
            jsonObj[key] = val
          } catch (error) {
            console.error(`Error calling getter ${key}`, error)
          }
        }
      })

    return jsonObj
  }
}
