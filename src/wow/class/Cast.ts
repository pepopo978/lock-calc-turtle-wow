import Character from './Character'
import OptionsInterface from '../interface/Options'
import Spell from './Spell'
import Target from './Target'
import Equipment from './Equipment'
import CastDmgObject from '../interface/CastDmgObject'
import MagicSchool from '../enum/MagicSchool'
import {
  BASE_SPELL_CRIT_MULTIPLIER,
  CORRUPTION_DURATION,
  CORRUPTION_MIN_TIME_LEFT_FOR_RECAST,
  CORRUPTION_TICK_DAMAGE,
  CORRUPTION_TICK_SP_COEFFICIENT,
  CURSE_OF_AGONY_DURATION,
  CURSE_OF_AGONY_MIN_TIME_LEFT_FOR_RECAST,
  CURSE_OF_AGONY_NUM_TICKS,
  CURSE_OF_AGONY_TICK_DAMAGE,
  CURSE_OF_AGONY_TICK_SP_COEFFICIENT,
  CURSE_OF_SHADOW_DURATION,
  CURSE_OF_SHADOW_MIN_TIME_LEFT_FOR_RECAST,
  GLOBAL_COOLDOWN,
  IMMOLATE_DIRECT_DAMAGE,
  IMMOLATE_DIRECT_DAMAGE_SP_COEFFICIENT,
  IMMOLATE_DURATION,
  IMMOLATE_MIN_TIME_LEFT_FOR_RECAST,
  IMMOLATE_TICK_DAMAGE,
  IMMOLATE_TICK_SP_COEFFICIENT,
  IMP_CRIT_CHANCE,
  INT_FOR_1_SPELL_CRIT_WARLOCK,
  SCALING_FACTOR,
  SPELL_CRIT_CAP,
  SPELL_HIT_CAP
} from '../module/Constants'
import { trinketDmgValues } from './Trinkets'
import Item from './Item'
import TrinketDisplayValues from '../interface/TrinketDisplayValues'
import HasteDisplayValues from '../interface/HasteDisplayValues'
import ManaData from '../interface/ManaData'
import TalentSchool from '@/wow/enum/TalentSchool'
import CastData from '@/wow/interface/CastData'
import SpellDisplayValues from '@/wow/interface/SpellDisplayValues'
import ISBDisplayValues from '@/wow/interface/ISBDisplayValues'
import ScoreInfo from '@/wow/interface/ScoreInfo'

interface EquipmentOverride {
  equipment?: Equipment
  spellHitWeight?: number
  spellCritWeight?: number
  spellPenetrationWeight?: number
  spellCastTime?: number
  spellCrit?: number
}

/**
 * A Spell cast by Character at Target.
 */
export default class Cast {
  spell: Spell
  target: Target
  character: Character
  options: OptionsInterface

  constructor(options: OptionsInterface, equipmentOverride?: EquipmentOverride) {
    /* By default gear is determined by Equipment(). We can override it by passing our own in.
     * If we don't pass our own equipment in, we can also override the stat weights used
     * by Equipment() to select the gear */
    let equipment: Equipment
    if (equipmentOverride && equipmentOverride.equipment) {
      equipment = equipmentOverride.equipment
    } else {
      equipment = new Equipment(
        options,
        equipmentOverride && equipmentOverride.spellHitWeight !== undefined
          ? equipmentOverride.spellHitWeight
          : undefined,
        equipmentOverride && equipmentOverride.spellCritWeight !== undefined
          ? equipmentOverride.spellCritWeight
          : undefined,
        equipmentOverride && equipmentOverride.spellPenetrationWeight !== undefined
          ? equipmentOverride.spellPenetrationWeight
          : undefined,
        equipmentOverride && equipmentOverride.spellCastTime !== undefined
          ? equipmentOverride.spellCastTime
          : undefined,
        equipmentOverride && equipmentOverride.spellCrit !== undefined ? equipmentOverride.spellCrit : undefined
      )
    }

    this.options = options
    this.character = new Character(options.character, equipment)
    this.spell = new Spell(options.spellName)
    this.target = new Target(options.target)

    this.updateScoreInfo(equipment.scoreInfo)
  }

  get magicSchoolForSpell(): MagicSchool {
    return this.spell.magicSchool
  }

  get talentSchoolForSpell(): TalentSchool {
    return this.spell.talentSchool
  }

  // any trinket that doesn't directly add spell power to each cast will be handled here
  get onUseTrinketEncounterDPS(): number {
    let dps = 0
    const trinket1 = this.character.equipment.items.trinket
    if (trinket1) {
      const values = trinketDmgValues(trinket1.itemJSON, {
        encLen: this.options.encLen,
        magicSchool: this.magicSchoolForSpell,
        targetType: this.target.options.type,
        castDmgEV: this.expectedDmgValuePerCast(),
        castLag: this.options.castLag,
        spellHaste: this.character.getEffectiveSpellHaste(),
        spellBaseCastTime: this.castTime,
        spellEffectiveCastTime: this.effectiveCastTime,
        spellPower: this.effectiveSpellPower(this.spell.magicSchool),
        spellPowerToDmgRatio: this.spellPowerToDmgRatio,
        spellCoefficient: this.spell.coefficient.direct
      })
      if (values.onUse) {
        dps += values.onUse.encounterDPS
      }
    }
    const trinket2 = this.character.equipment.items.trinket2
    if (trinket2) {
      const values = trinketDmgValues(trinket2.itemJSON, {
        encLen: this.options.encLen,
        magicSchool: this.magicSchoolForSpell,
        targetType: this.target.options.type,
        castDmgEV: this.expectedDmgValuePerCast(),
        castLag: this.options.castLag,
        spellHaste: this.character.getEffectiveSpellHaste(),
        spellBaseCastTime: this.castTime,
        spellEffectiveCastTime: this.effectiveCastTime,
        spellPower: this.effectiveSpellPower(this.spell.magicSchool),
        spellPowerToDmgRatio: this.spellPowerToDmgRatio,
        spellCoefficient: this.spell.coefficient.direct
      })
      if (values.onUse) {
        dps += values.onUse.encounterDPS
      }
    }

    return dps
  }

  get trinketDisplayValues(): TrinketDisplayValues[] {
    let displayValues = [] as TrinketDisplayValues[]

    const trinket1 = this.character.equipment.items.trinket
    if (trinket1) {
      displayValues = displayValues.concat(this._getTrinketDisplayValues(trinket1))
    }

    const trinket2 = this.character.equipment.items.trinket2
    if (trinket2) {
      displayValues = displayValues.concat(this._getTrinketDisplayValues(trinket2))
    }
    return displayValues
  }

  get hasTrinkets(): boolean {
    // check trinkets only for now
    const trinket1 = this.character.equipment.items.trinket
    const trinket2 = this.character.equipment.items.trinket2

    return trinket1 !== undefined || trinket2 !== undefined
  }

  get hasHaste(): boolean {
    return this.character.getEffectiveSpellHaste() > 0
  }

  get hasteDisplayValues(): HasteDisplayValues {
    const hastePercent = this.character.getEffectiveSpellHaste()
    const totalSBCasts = this.numSBCasts
    const sbEncLen = totalSBCasts * this.effectiveCastTime // actual time spent casting shadow bolt during encounter

    const partialCastsWithoutHaste = Math.round((100 * sbEncLen) / this.effectiveCastTimeWithoutHaste) / 100
    const partialCastsWithHaste = Math.round((100 * sbEncLen) / this.effectiveCastTime) / 100

    const baseCastTime = this.castTime
    const castTimeReduction = (baseCastTime * hastePercent) / 100

    let castsWithoutHaste = partialCastsWithoutHaste
    let castsWithHaste = partialCastsWithHaste

    if (!this.options.partialHasteCasts) {
      castsWithoutHaste = Math.floor(castsWithoutHaste)
      castsWithHaste = Math.floor(castsWithHaste)
    }

    // calculate additional casts
    const additionalCasts = Math.round(100 * (castsWithHaste - castsWithoutHaste)) / 100

    const totalDmg = Math.round(additionalCasts * this.expectedDmgValuePerCast())
    const totalSP = totalDmg / this.spellPowerToDmgRatio
    const avgSPPerCast = totalSP / totalSBCasts
    const encounterDPS = totalDmg / this.options.encLen

    return {
      hastePercent: hastePercent,
      castTimeReduction: castTimeReduction,
      partialCastsWithoutHaste: partialCastsWithoutHaste,
      partialCastsWithHaste: partialCastsWithHaste,
      additionalCasts: additionalCasts,
      totalDmg: totalDmg,
      avgSPPerCast: avgSPPerCast,
      encounterDPS: encounterDPS
    }
  }

  get effectiveImpFireboltCastTime(): number {
    return 2 - this.character.impFireboltCastTimeReduction
  }

  get ISBDisplayValues(): ISBDisplayValues {
    const castData = this.castData()
    const critChance = this.character.getEffectiveSpellCrit(TalentSchool.Destruction, this.spell.name)
    const critProbability = critChance / 100
    const roundedCrit = Math.round(10 * critChance) / 10
    const numSBCasts = castData.casts.shadowBolt

    // fix fp rounding issues
    const isbDamageBonus = Math.round(10 * (this.character.getImprovedShadowBoltShadowDmgBonus(MagicSchool.Shadow) - 1)) / 10

    let numProcs = 0
    let numSBCastsISBActive = 0
    for (let i = 0; i < numSBCasts; i++) {
      numProcs += critProbability

      // calculate sb uptime
      if (i === 0) {
        // first cast, not possible to have ISB active
      } else {
        // for all other casts, use the crit chance from up to the previous 4 casts
        // to determine if ISB was active
        const numPreviousCasts = Math.min(i, 4)
        numSBCastsISBActive += numPreviousCasts * critProbability
      }
    }
    const addedSBDamage = numSBCastsISBActive *
      isbDamageBonus *
      this.expectedDmgValuePerCast() *
      this.chanceToHit(this.talentSchoolForSpell) / 100
    const uptimePercentage = numSBCastsISBActive / numSBCasts

    const uptimeScalingFactor = uptimePercentage * isbDamageBonus

    let agonyDmg = this.effectiveCurseOfAgonyTotalDmg(castData.ticks.curseOfAgony)
    // first coa tick will never have isb, ignore it
    // subtract a tick if using corruption or immolate
    const ineligibleCoaTicks = Math.min(1 + (this.options.cor ? 1 : 0) + (this.options.imm ? 1 : 0), 0)
    // subtract ineligible ticks
    agonyDmg -= ineligibleCoaTicks * this.effectiveCurseOfAgonyTickDmg(0)
    const addedCoaDmg = agonyDmg * uptimeScalingFactor

    // first corruption tick will never have isb, ignore it
    // subtract a tick if using immolate
    const eligibleCorruptionTicks = Math.max(castData.ticks.corruption - (this.options.imm ? 1 : 0) - 1, 0)
    const addedCorruptionDmg = eligibleCorruptionTicks * this.effectiveCorruptionTickDmg() * uptimeScalingFactor

    // approximate the added dot damage from ISB using uptime
    const addedDotDmg = addedCorruptionDmg + addedCoaDmg
    const totalDmg = addedDotDmg + addedSBDamage

    const encounterDPS = totalDmg / this.options.encLen

    return {
      desc: castData.casts.shadowBolt + ' SB casts | ' + roundedCrit + '% crit',
      numProcs: Math.round(10 * numProcs) / 10,
      castsISBActive: Math.round(10 * numSBCastsISBActive) / 10,
      uptimePercentage: Math.round(100 * 10 * uptimePercentage) / 10,
      addedDotDmg: addedDotDmg,
      addedSBDmg: addedSBDamage,
      totalDmg: totalDmg,
      encounterDPS: encounterDPS
    }
  }

  get hasDots(): boolean {
    return this.options.cor || this.options.coa || this.options.imm || this.options.cos
  }

  get hasISB(): boolean {
    return this.character.getImprovedShadowBoltShadowDmgBonus(MagicSchool.Shadow) > 1
  }

  get numTotalCasts(): number {
    return this.spellDisplayValues().map(spell => spell.totalCasts).reduce((prev, next) => prev + next)
  }

  get numSBCasts(): number {
    return this.castData().casts.shadowBolt
  }

  get numTicks() {
    return Math.floor(this.options.encLen / 2)
  }

  get manaCostPerCast(): number {
    // todo handle cataclysm
    return this.spell.manaCost
  }

  get manaUsed(): number {
    return this.numSBCasts * this.manaCostPerCast
  }

  get manaData(): ManaData {
    const manaGemAndTeaCD = 120
    const potionCD = 120
    const maxMana = this.character.mana

    const manaPotionMana = 1800
    const teaMana = 1400

    let currentPotionCD = 0
    let currentGemAndTeaCD = 0

    let currentMana = this.character.mana
    let currentCast = 0

    let manaPotionsUsed = 0
    let teasUsed = 0

    let wentOOM = false

    for (let i = 0; i < this.options.encLen; i++) {
      const missingMana = maxMana - currentMana

      // check if we need to use mana gem / tea
      // todo add lifetap
      if (this.options.useTea && missingMana >= teaMana && currentGemAndTeaCD <= 0) {
        teasUsed++
        currentMana += teaMana
        currentGemAndTeaCD = manaGemAndTeaCD
      }

      // check if we need to use mana potion
      if (this.options.useMp && missingMana >= manaPotionMana && currentPotionCD <= 0) {
        manaPotionsUsed++
        currentMana += manaPotionMana
        currentPotionCD = potionCD
      }

      const numTheoreticalCasts = Math.floor(i / this.effectiveCastTime)
      if (numTheoreticalCasts > currentCast) {
        if (currentMana < 0) {
          wentOOM = true
        }

        // new cast occurred
        currentCast = numTheoreticalCasts
        currentMana -= this.manaCostPerCast
      }

      // lower all CDs
      currentGemAndTeaCD--
      currentPotionCD--
    }

    return {
      manaPotionsUsed: manaPotionsUsed,
      manaPotionsRestored: manaPotionsUsed * manaPotionMana,
      teasUsed: teasUsed,
      teasRestored: teasUsed * teaMana,
      finalMana: currentMana,
      wentOOM: wentOOM
    }
  }

  get curseOfShadowResistBonus(): number {
    return this.spell.isShadow ? this.target.curseOfShadowResistBonus : 0
  }

  get curseOfElementsResistBonus(): number {
    return this.spell.isFire || this.spell.isFrost ? this.target.curseOfElementsResistBonus : 0
  }

  /**
   * Effect #1	Apply Aura: Mod % Damage Taken (All)
   * Value: -75%
   * Effect #2	Apply Aura: Mod % Damage Taken (Vulnerable)
   * Value: 1100%
   *
   */
  get shimmerBonus(): number {
    const modifier = this.target.shimmer > 0 ? 1 - 0.75 : 1
    return this.target.shimmer === this.spell.magicSchool ? modifier * (1 + 11) : modifier
  }

  get effectiveTargetResistance(): number {
    const resistance = Math.min(this.target.spellResistance, 5 * this.character.level - this.targetResistanceFromLevel)
    return resistance - Math.min(this.spellPenetration, resistance) + this.targetResistanceFromLevel
  }

  /* For non-binary spells only: Each difference in level gives a 2% resistance chance that cannot
   * be negated (by spell penetration or otherwise). */
  get targetResistanceFromLevel(): number {
    if (this.spell.isBinary) {
      return 0
    }
    return (
      (this.target.level > this.character.level ? this.target.level - this.character.level : 0) *
      parseFloat((0.1333 * this.character.level).toFixed(2))
    )
  }

  /* https://dwarfpriest.wordpress.com/2008/01/07/spell-hit-spell-penetration-and-resistances/#more-176 */
  get partialResistPenalty(): number {
    return this.spell.canPartialResist ? (0.75 * this.effectiveTargetResistance) / (5 * this.character.level) : 0
  }

  get baseDmgMultiplier(): number {
    return 1
  }

  /**
   * Mitigates spell resist of SpellCast. Needs work.
   */
  get spellPenetration(): number {
    switch (this.spell.magicSchool) {
      case MagicSchool.Arcane:
      case MagicSchool.Shadow:
        return this.character.equipment.spellPenetration + this.target.curseOfShadowResistBonus
      case MagicSchool.Nature:
        return this.character.equipment.spellPenetration
      case MagicSchool.Fire:
      case MagicSchool.Frost:
        return this.character.equipment.spellPenetration + this.target.curseOfElementsResistBonus
      default:
        return this.character.equipment.spellPenetration
    }
  }

  /**
   * Spell cast time . Factors in talents that modify base spell cast time.
   */
  get castTime(): number {
    let castTime = this.spell.castTime <= GLOBAL_COOLDOWN ? GLOBAL_COOLDOWN : this.spell.castTime

    if (this.spell.name.includes('Shadow Bolt') || this.spell.name.includes('Immolate')) {
      castTime = this.spell.castTime - this.character.baneShadowboltCastTimeReduction
    }

    return castTime + this.options.castLag
  }

  /**
   * Factors in cast speed, procs like natures grace, hit, crit and "human factor" (which might actually be latency?)
   */
  get effectiveCastTimeWithoutHaste(): number {
    if (this.character.buffs.hasBurningAdrenaline) {
      return GLOBAL_COOLDOWN + this.options.castLag
    }

    return Math.max(GLOBAL_COOLDOWN + this.options.castLag, this.castTime)
  }

  get effectiveCastTime(): number {
    const spellHaste = this.character.getEffectiveSpellHaste()
    const castTimeWithoutLag = this.effectiveCastTimeWithoutHaste - this.options.castLag

    return (
      Math.max(GLOBAL_COOLDOWN + this.options.castLag, castTimeWithoutLag / (1 + spellHaste / 100)) +
      this.options.castLag
    )
  }

  /**
   * Chance of missing a spell
   *
   */
  get chanceToMiss(): number {
    return Math.max(1, 100 - this.chanceToHit(this.spell.talentSchool))
  }

  /**
   * The bonus multiplier of a crit, not counting the base
   */
  get critBonusMultiplier(): number {
    return this.critMultiplier(this.spell.talentSchool) - 1
  }

  /**
   * spell crit weight i.e. the amount of spell power 1 point of crit is worth.
   */
  get spellCritWeight(): number {
    return this.effectiveSpellCrit(this.talentSchoolForSpell) < SPELL_CRIT_CAP ? this.spellCritToSpellDamage : 0
  }

  get spellHasteWeight(): number {
    return Item.calculateHasteScore(1, this.character.equipment.scoreInfo)
  }

  /**
   * spell hit weight i.e. the amount of spell power 1 point of hit is worth.
   */
  get spellHitWeight(): number {
    return this.chanceToMiss > 1 ? this.spellHitToSpellDamage : 0
  }

  /**
   * int weight i.e. the amount of spell power 1 point of int is worth
   */
  get intWeight(): number {
    return this.spellCritWeight > 0 ? this.spellCritWeight / INT_FOR_1_SPELL_CRIT_WARLOCK : 0
  }

  /**
   * spell pen weight i.e the amount of spell power 1 point of spell penetration is worth
   */

  get spellPenetrationWeight(): number {
    // if the boss only has unmitigatable level based resistances left after spell pen, then it's value is 0
    if (this.effectiveTargetResistance <= this.targetResistanceFromLevel) {
      return 0
    }

    // B == Spell Base damage
    // m == base damage modifier (moonfury)
    // c == spell coefficient
    // P == Spellpower
    const m = 1
    const B = this.normalDmg().base.avg
    const c = this.spell.coefficient.direct
    const P = this.baseSpellPower(this.spell.magicSchool)
    const BossResist = this.target.spellResistance
    const SpellPen = this.spellPenetration
    const result = (m * B + c * P) / (c * (400 - (BossResist - SpellPen)))

    return result
  }

  get spellPowerToDmgRatio() {
    const baseDmg = this.spellDisplayValues().map(item => item.totalDmg).reduce((prev, next) => prev + next)
    const extraSPDmg = this.spellDisplayValues(0, 0, 1).map(item => item.totalDmg).reduce((prev, next) => prev + next)
    const addedDmgPerCast = (extraSPDmg - baseDmg) / this.numTotalCasts

    return addedDmgPerCast
  }

  get spellCritToSpellDamage(): number {
    const baseDmg = this.spellDisplayValues().map(item => item.totalDmg).reduce((prev, next) => prev + next)
    const extraCritDmg = this.spellDisplayValues(0, 1).map(item => item.totalDmg).reduce((prev, next) => prev + next)
    const addedDmgPerCast = (extraCritDmg - baseDmg) / this.numTotalCasts

    // convert to spell power
    return addedDmgPerCast / this.spellPowerToDmgRatio
  }

  get spellHitToSpellDamage(): number {
    const baseDmg = this.spellDisplayValues().map(item => item.totalDmg).reduce((prev, next) => prev + next)
    const extraHitDmg = this.spellDisplayValues(1).map(item => item.totalDmg).reduce((prev, next) => prev + next)
    const addedDmgPerCast = (extraHitDmg - baseDmg) / this.numTotalCasts

    // console.log(this.spellDisplayValues())
    // console.log(this.spellDisplayValues(1))
    // console.log('-----------------')

    // convert to spell power
    return addedDmgPerCast / this.spellPowerToDmgRatio
  }

  buffSpellPower(magicSchool: MagicSchool): number {
    return (
      this.character.buffs.flaskOfSupremePowerBonus +
      this.character.buffs.telAbimDelightSPBonus +
      this.character.buffs.dreamshardElixirSPBonus +
      this.character.buffs.dreamtonicSPBonus +
      this.character.buffs.greaterArcaneElixirBonus +
      this.character.buffs.brilliantWizardOilSpellDamageBonus +
      this.character.buffs.warlockAtieshSpellDamageBonus +
      this.character.buffs.blessedWizardOilSpellDamageBonus(this.target.options.type) +
      this.character.buffs.getElixirOfFrostPowerDamageBonus(magicSchool) +
      this.character.buffs.getElixirOfGreaterFirePowerDamageBonus(magicSchool) +
      this.character.buffs.getElixirOfShadowPowerDamageBonus(magicSchool)
    )
  }

  baseSpellPower(magicSchool: MagicSchool): number {
    switch (magicSchool) {
      case MagicSchool.Physical:
        return 0
      case MagicSchool.Arcane:
        return this.character.arcaneSpellPower + this.character.spellPower
      case MagicSchool.Nature:
        return this.character.natureSpellPower + this.character.spellPower
      case MagicSchool.Fire:
        return this.character.fireSpellPower + this.character.spellPower
      case MagicSchool.Frost:
        return this.character.frostSpellPower + this.character.spellPower
      case MagicSchool.Shadow:
      case MagicSchool.Holy:
      default:
        return this.character.spellPower
    }
  }

  effectiveSpellPower(magicSchool: MagicSchool): number {
    return this.baseSpellPower(magicSchool) + this.buffSpellPower(magicSchool)
  }

  effectiveSpellCrit(talentSchool: TalentSchool): number {
    return Math.min(this.character.getEffectiveSpellCrit(talentSchool, this.spell.name), SPELL_CRIT_CAP)
  }

  critDmg(bonusSP: number = 0): CastDmgObject {
    const critDmgObj = {} as CastDmgObject
    const normalDmgObj = this.normalDmg(bonusSP)

    const _critDmg = (dmg: number) => {
      return dmg * this.critMultiplier(this.spell.talentSchool)
    }

    critDmgObj.base = this._dmgData(_critDmg, normalDmgObj.base.min, normalDmgObj.base.max, normalDmgObj.base.avg)
    critDmgObj.actual = this._dmgData(
      _critDmg,
      normalDmgObj.actual.min,
      normalDmgObj.actual.max,
      normalDmgObj.actual.avg
    )
    critDmgObj.effective = this._dmgData(
      _critDmg,
      normalDmgObj.effective.min,
      normalDmgObj.effective.max,
      normalDmgObj.effective.avg
    )

    return critDmgObj
  }

  expectedDmgValuePerCast(bonusCrit: number = 0, bonusSP: number = 0) {
    const normalChance = this.chanceToNormal(this.spell.talentSchool)
    const critChance = this.chanceToCrit(this.spell.talentSchool, bonusCrit)

    const normalDmg = this.normalDmg(bonusSP).effective.avg
    let critDmg = this.critDmg(bonusSP).effective.avg

    return normalChance * normalDmg + critChance * critDmg
  }

  normalDmg(bonusSP: number = 0): CastDmgObject {
    const dmgObj = {} as CastDmgObject

    const _baseDmg = (dmg: number) => {
      return dmg * this.baseDmgMultiplier
    }

    const _actualDmg = (dmg: number) => {
      return dmg + this.spell.coefficient.direct * (this.effectiveSpellPower(this.spell.magicSchool) + bonusSP)
    }

    const _effectiveDmg = (dmg: number) => {
      return dmg * this.effectiveDmgMultiplier(this.spell.magicSchool)
    }

    dmgObj.base = this._dmgData(_baseDmg, this.spell.minDmg, this.spell.maxDmg, this.spell.avgDmg)
    dmgObj.actual = this._dmgData(_actualDmg, dmgObj.base.min, dmgObj.base.max, dmgObj.base.avg)
    dmgObj.effective = this._dmgData(_effectiveDmg, dmgObj.actual.min, dmgObj.actual.max, dmgObj.actual.avg)
    return dmgObj
  }

  effectiveCorruptionTickDmg(bonusSP: number = 0) {
    return this.effectiveDotTickDmg(
      CORRUPTION_TICK_DAMAGE,
      CORRUPTION_TICK_SP_COEFFICIENT,
      this.effectiveSpellPower(MagicSchool.Shadow) + bonusSP,
      MagicSchool.Shadow)
  }

  effectiveImmolateTickDmg(bonusSP: number = 0) {
    return this.effectiveDotTickDmg(
      IMMOLATE_TICK_DAMAGE,
      IMMOLATE_TICK_SP_COEFFICIENT,
      this.effectiveSpellPower(MagicSchool.Fire) + bonusSP,
      MagicSchool.Fire)
  }

  /**
   * Chance of critting with a spell
   *
   */
  chanceToCrit(talentSchool: TalentSchool, bonusCrit: number = 0): number {
    const critChance = this.character.getEffectiveSpellCrit(talentSchool, this.spell.name) + bonusCrit
    return critChance / 100
  }

  /**
   * Chance of landing a Normal spell hit i.e. not a crit
   *
   */
  chanceToNormal(talentSchool: TalentSchool): number {
    return 1 - this.chanceToCrit(talentSchool)
  }

  critMultiplier(talentSchool: TalentSchool): number {
    return BASE_SPELL_CRIT_MULTIPLIER + this.talentSchoolCritMultiplier(talentSchool)
  }

  talentSchoolCritMultiplier(talentSchool: TalentSchool): number {
    switch (talentSchool) {
      case TalentSchool.Destruction:
        return this.character.ruinCritBonusDamage
      default:
        return 0
    }
  }

  castData(bonusHit: number = 0): CastData {
    const lagTime = this.options.castLag * SCALING_FACTOR
    // don't factor in resists for shadow bolt as you won't always recast if it resists
    const shadowBoltCastTime = Math.round(this.effectiveCastTime * SCALING_FACTOR) // lag already included in effectiveCastTime
    const instantCastTimeWithResists = Math.round(this.effectiveAfflictionInstantCastTime(bonusHit) * SCALING_FACTOR) + lagTime
    const immolateCastTimeWithResists = Math.round(this.effectiveImmolateCastTime(bonusHit) * SCALING_FACTOR) + lagTime
    const corruptionCastTimeWithResists = Math.round(this.effectiveCorruptionCastTime(bonusHit) * SCALING_FACTOR) + lagTime
    const encLen = this.options.encLen * SCALING_FACTOR // scale to 100x to avoid dealing with floating point numbers
    const stepSize = 1 // .01 second since encLen is scaled 100x
    const usePartialTicks = this.options.partialTicks

    const casts = {
      shadowBolt: 0,
      curseOfShadow: 0,
      curseOfAgony: 0,
      corruption: 0,
      immolate: 0
    }

    const ticks = {
      shadowBolt: 0,
      curseOfShadow: 0,
      curseOfAgony: 0,
      corruption: 0,
      immolate: 0
    }

    const debuffRemainingDurations = {
      shadowBolt: -1,
      curseOfShadow: -1,
      curseOfAgony: -1,
      corruption: -1,
      immolate: -1
    }

    const THREE_SECONDS = 3 * SCALING_FACTOR
    const TWO_SECONDS = 2 * SCALING_FACTOR

    let currentTime = 0

    // tslint:disable-next-line:no-inner-declarations
    function processTime(timeToProcess: number) {
      let endOfEncounter = false
      // see if we can process full time
      if (timeToProcess + currentTime >= encLen) {
        // reduce time to the end of the encounter
        timeToProcess = Math.max(encLen - currentTime, stepSize)
        endOfEncounter = true
      }

      for (let i = stepSize; i <= timeToProcess; i += 1) {
        // advance by stepSize seconds rounding to 1 decimal place
        currentTime += stepSize

        // process ticks
        // Curse of shadow
        if (debuffRemainingDurations.curseOfShadow >= 0) {
          debuffRemainingDurations.curseOfShadow -= stepSize
        }

        // immolate
        if (debuffRemainingDurations.immolate >= 0) {
          // immolate is every 3 seconds
          if (debuffRemainingDurations.immolate !== IMMOLATE_DURATION && debuffRemainingDurations.immolate % THREE_SECONDS === 0) {
            ticks.immolate++
          }
          debuffRemainingDurations.immolate -= stepSize
        }
        // corruption
        if (debuffRemainingDurations.corruption >= 0) {
          // corruption is every 3 seconds
          if (
            debuffRemainingDurations.corruption !== CORRUPTION_DURATION &&
            debuffRemainingDurations.corruption % THREE_SECONDS === 0
          ) {
            ticks.corruption++
          }
          debuffRemainingDurations.corruption -= stepSize
        }
        // curse of agony
        if (debuffRemainingDurations.curseOfAgony >= 0) {
          // curse of agony is every 2 seconds
          if (
            debuffRemainingDurations.curseOfAgony !== CURSE_OF_AGONY_DURATION &&
            debuffRemainingDurations.curseOfAgony % TWO_SECONDS === 0
          ) {
            ticks.curseOfAgony++
          }
          debuffRemainingDurations.curseOfAgony -= stepSize
        }
      }
      if (usePartialTicks && endOfEncounter) {
        if (debuffRemainingDurations.immolate >= 0) {
          const partialImmolate = THREE_SECONDS - debuffRemainingDurations.immolate % THREE_SECONDS
          ticks.immolate += partialImmolate / THREE_SECONDS
          ticks.immolate = Math.round(10 * ticks.immolate) / 10
        }

        if (debuffRemainingDurations.corruption >= 0) {
          const partialCorruption = THREE_SECONDS - debuffRemainingDurations.corruption % THREE_SECONDS
          ticks.corruption += partialCorruption / THREE_SECONDS
          ticks.corruption = Math.round(10 * ticks.corruption) / 10
        }

        if (debuffRemainingDurations.curseOfAgony >= 0) {
          const partialCoA = TWO_SECONDS - debuffRemainingDurations.curseOfAgony % TWO_SECONDS
          ticks.curseOfAgony += partialCoA / TWO_SECONDS
          ticks.curseOfAgony = Math.round(10 * ticks.curseOfAgony) / 10
        }
      }
    }

    for (; currentTime < encLen;) {
      const remainingTime = encLen - currentTime
      // decide which spell to cast
      if (this.options.cos && debuffRemainingDurations.curseOfShadow <= 0 && remainingTime >= CURSE_OF_SHADOW_MIN_TIME_LEFT_FOR_RECAST) {
        debuffRemainingDurations.curseOfShadow = CURSE_OF_SHADOW_DURATION
        casts.curseOfShadow++
        // wait for gcd
        processTime(instantCastTimeWithResists)
      } else if (this.options.coa && debuffRemainingDurations.curseOfAgony <= 0 && remainingTime >= CURSE_OF_AGONY_MIN_TIME_LEFT_FOR_RECAST) {
        debuffRemainingDurations.curseOfAgony = CURSE_OF_AGONY_DURATION
        casts.curseOfAgony++
        // wait for gcd
        processTime(instantCastTimeWithResists)
      } else if (this.options.cor && debuffRemainingDurations.corruption <= 0 && remainingTime >= CORRUPTION_MIN_TIME_LEFT_FOR_RECAST) {
        debuffRemainingDurations.corruption = CORRUPTION_DURATION
        casts.corruption++
        // wait for gcd
        processTime(corruptionCastTimeWithResists)
      } else if (this.options.imm && debuffRemainingDurations.immolate <= 0 && remainingTime >= IMMOLATE_MIN_TIME_LEFT_FOR_RECAST + immolateCastTimeWithResists) {
        // immolate has a cast time so process first
        processTime(immolateCastTimeWithResists)
        debuffRemainingDurations.immolate = IMMOLATE_DURATION
        casts.immolate++
      } else {
        // default to shadowbolt
        processTime(shadowBoltCastTime)
        if (remainingTime >= shadowBoltCastTime) {
          casts.shadowBolt++
        } else if (this.options.partialCasts) {
          casts.shadowBolt += Math.round(10 * remainingTime / shadowBoltCastTime) / 10
        }
      }
    }

    return {
      casts: casts,
      ticks: ticks
    }
  }

  spellDisplayValues(bonusHit: number = 0, bonusCrit: number = 0, bonusSP: number = 0): SpellDisplayValues[] {
    const castData = this.castData(bonusHit)
    let displayValues = [] as SpellDisplayValues[]

    if (this.options.cos) {
      displayValues.push({
        name: 'Curse of Shadow',
        effectiveCastTime: this.effectiveAfflictionInstantCastTime(bonusHit),
        effectiveTickDmg: 0,
        totalCasts: castData.casts.curseOfShadow,
        totalTicks: castData.ticks.curseOfShadow,
        totalDotDmg: 0,
        totalDirectDmg: 0,
        totalDmg: 0,
        encounterDPS: 0
      })
    }

    if (this.options.coa) {
      // get avg tick dmg for 1 full duration
      const coaEffectiveTickDmg = this.effectiveCurseOfAgonyTotalDmg(CURSE_OF_AGONY_NUM_TICKS, bonusSP) / CURSE_OF_AGONY_NUM_TICKS

      const totalDotDmg = this.effectiveCurseOfAgonyTotalDmg(castData.ticks.curseOfAgony, bonusSP)

      displayValues.push({
        name: 'Curse of Agony',
        effectiveCastTime: this.effectiveAfflictionInstantCastTime(bonusHit),
        effectiveTickDmg: coaEffectiveTickDmg,
        totalCasts: castData.casts.curseOfAgony,
        totalTicks: castData.ticks.curseOfAgony,
        totalDotDmg: totalDotDmg,
        totalDirectDmg: 0,
        totalDmg: totalDotDmg,
        encounterDPS: totalDotDmg / this.options.encLen
      })
    }

    if (this.options.cor) {
      const corEffectiveTickDmg = this.effectiveCorruptionTickDmg(bonusSP)
      const totalDotDmg = corEffectiveTickDmg * castData.ticks.corruption

      displayValues.push({
        name: 'Corruption',
        effectiveCastTime: this.effectiveCorruptionCastTime(bonusHit),
        effectiveTickDmg: corEffectiveTickDmg,
        totalCasts: castData.casts.corruption,
        totalTicks: castData.ticks.corruption,
        totalDotDmg: totalDotDmg,
        totalDirectDmg: 0,
        totalDmg: totalDotDmg,
        encounterDPS: totalDotDmg / this.options.encLen
      })
    }

    if (this.options.imm) {
      const immEffectiveTickDmg = this.effectiveImmolateTickDmg()
      const totalDotDmg = immEffectiveTickDmg * castData.ticks.immolate

      const baseDirectDmg = IMMOLATE_DIRECT_DAMAGE * this.character.improvedImmolateDamageBonus
      const immNormalDmg = (baseDirectDmg + IMMOLATE_DIRECT_DAMAGE_SP_COEFFICIENT * this.effectiveSpellPower(MagicSchool.Fire)) *
        this.effectiveDmgMultiplier(MagicSchool.Fire)
      const immCritDmg = immNormalDmg * this.critMultiplier(TalentSchool.Destruction)
      const totalDirectDmg = castData.casts.immolate *
        (immNormalDmg * this.chanceToNormal(TalentSchool.Destruction) + immCritDmg * this.chanceToCrit(TalentSchool.Destruction, bonusCrit))

      displayValues.push({
        name: 'Immolate',
        effectiveCastTime: this.effectiveImmolateCastTime(bonusHit),
        effectiveTickDmg: immEffectiveTickDmg,
        totalCasts: castData.casts.immolate,
        totalTicks: castData.ticks.immolate,
        totalDotDmg: totalDotDmg,
        totalDirectDmg: totalDirectDmg,
        totalDmg: totalDotDmg + totalDirectDmg,
        encounterDPS: (totalDotDmg + totalDirectDmg) / this.options.encLen
      })
    }

    const totalSBDmg = this.expectedDmgValuePerCast(bonusCrit, bonusSP) *
      castData.casts.shadowBolt *
      this.chanceToHit(this.spell.talentSchool, bonusHit) / 100

    displayValues.push({
      name: 'Shadow Bolt',
      effectiveCastTime: this.effectiveCastTime,
      effectiveTickDmg: 0,
      totalCasts: castData.casts.shadowBolt,
      totalTicks: 0,
      totalDotDmg: 0,
      totalDirectDmg: totalSBDmg,
      totalDmg: totalSBDmg,
      encounterDPS: totalSBDmg / this.options.encLen
    })

    if (this.options.imp) {
      const impFireboltCastTime = this.effectiveImpFireboltCastTime
      const numImpCasts = Math.floor(this.options.encLen / impFireboltCastTime)
      const impDmgPerNormalCast = 88.5 *
        this.target.getScorchVulnerabilityFireDmgBonus(MagicSchool.Fire) *
        this.target.curseOfElementsDamageBonus
      const impDmgPerCritCast = impDmgPerNormalCast * BASE_SPELL_CRIT_MULTIPLIER

      const hitProbability = this.target.hitChance / 100
      const normalHitChance = (1 - IMP_CRIT_CHANCE)
      const impDmg = hitProbability * numImpCasts * (impDmgPerNormalCast * normalHitChance + impDmgPerCritCast * IMP_CRIT_CHANCE)

      displayValues.push({
        name: 'Imp Firebolt',
        effectiveCastTime: impFireboltCastTime,
        effectiveTickDmg: 0,
        totalCasts: numImpCasts,
        totalTicks: 0,
        totalDotDmg: 0,
        totalDirectDmg: impDmg,
        totalDmg: impDmg,
        encounterDPS: impDmg / this.options.encLen
      })
    }

    return displayValues
  }

  /**
   * Chance of hitting with a spell
   *
   */
  chanceToHit(talentSchool: TalentSchool, bonusHit: number = 0): number {
    return Math.min(SPELL_HIT_CAP, this.target.hitChance + this.character.getEffectiveSpellHit(talentSchool) + bonusHit)
  }

  effectiveDotCastTime(baseCastTime: number, talentSchool: TalentSchool, bonusHit: number): number {
    const resistChance = (100 - this.chanceToHit(talentSchool, bonusHit)) / 100
    let castTime = baseCastTime + this.options.castLag

    let currentResistChance = resistChance

    // calculate effect on cast time of up to 5 resists in a row
    for (let numResists = 1; numResists <= 5; numResists++) {
      castTime += castTime * currentResistChance

      // calculate next consecutive resist chance
      currentResistChance *= resistChance
    }

    return castTime
  }

  // factor in resists to calculate an average cast time for curses/dots
  effectiveAfflictionInstantCastTime(bonusHit: number): number {
    return this.effectiveDotCastTime(GLOBAL_COOLDOWN, TalentSchool.Affliction, bonusHit)
  }

  effectiveCorruptionCastTime(bonusHit: number): number {
    const corruptionCastTime = Math.max(2 - this.character.improvedCorruptionTimeReduction, GLOBAL_COOLDOWN)
    return this.effectiveDotCastTime(corruptionCastTime, TalentSchool.Affliction, bonusHit)
  }

  effectiveImmolateCastTime(bonusHit: number): number {
    const immolateCastTime = Math.min(2 - this.character.baneImmolateCastTimeReduction, GLOBAL_COOLDOWN)
    return this.effectiveDotCastTime(immolateCastTime, TalentSchool.Destruction, bonusHit)
  }

  effectiveDmgMultiplier(magicSchool: MagicSchool): number {
    return (
      this.character.buffs.powerInfusionDmgPercentBonus *
      this.character.buffs.saygesDarkFortuneBonus *
      this.character.buffs.tracesOfSilithystBonus *
      this.target.spellVulnerabilityBonus *
      this.target.getScorchVulnerabilityFireDmgBonus(magicSchool) *
      this.character.getImprovedShadowBoltShadowDmgBonus(magicSchool) *
      this.character.getShadowMasteryShadowDmgBonus(magicSchool) *
      this.character.getDemonicSacrificeShadowDmgBonus(magicSchool) *
      this.character.emberstormDamageBonus(magicSchool) *
      this.curseOfShadowDamageBonus(magicSchool) *
      this.curseOfElementsDamageBonus(magicSchool) *
      this.character.buffs.burningAdrenalineDamageBonus *
      this.shimmerBonus *
      (1 - this.partialResistPenalty)
    )
  }

  curseOfShadowDamageBonus(magicSchool: MagicSchool): number {
    return magicSchool === MagicSchool.Shadow ? this.target.curseOfShadowDamageBonus : 1.0
  }

  curseOfElementsDamageBonus(magicSchool: MagicSchool): number {
    return magicSchool === MagicSchool.Fire || magicSchool === MagicSchool.Frost ? this.target.curseOfElementsDamageBonus : 1.0
  }

  effectiveCurseOfAgonyTotalDmg(numTicks: number, bonusSP: number = 0): number {
    const fullDurationDmg = this.effectiveDotTickDmg(
      CURSE_OF_AGONY_TICK_DAMAGE,
      CURSE_OF_AGONY_TICK_SP_COEFFICIENT,
      this.effectiveSpellPower(MagicSchool.Shadow) + bonusSP,
      MagicSchool.Shadow) * CURSE_OF_AGONY_NUM_TICKS

    // calc full duration damage
    const numFullDurations = Math.floor(numTicks / CURSE_OF_AGONY_NUM_TICKS)
    let totalDmg = fullDurationDmg * numFullDurations

    // calc remaining ticks
    const remainingTicks = numTicks % CURSE_OF_AGONY_NUM_TICKS
    for (let i = 1; i <= remainingTicks; i++) {
      totalDmg += this.effectiveCurseOfAgonyTickDmg(i, bonusSP)
    }
    return totalDmg
  }

  effectiveCurseOfAgonyTickDmg(tickNumber: number, bonusSP: number = 0) {
    // first four ticks each deal 1/24 of the damage each (about 4.2%),
    // the next four deal 1/12 of the damage (about 8.3%),
    // and then the last four each deal 1/8 of the damage (12.5%).
    // In other words, the first four ticks combine to 1/6 (16.6%) of the damage,
    // the next four to 1/3 (33.3%), and the last four together to
    // about one half of the total damage.
    const baseTickDmg = this.effectiveDotTickDmg(
      CURSE_OF_AGONY_TICK_DAMAGE,
      CURSE_OF_AGONY_TICK_SP_COEFFICIENT,
      this.effectiveSpellPower(MagicSchool.Shadow) + bonusSP,
      MagicSchool.Shadow)
    const totalDmg = baseTickDmg * CURSE_OF_AGONY_NUM_TICKS

    if (tickNumber <= 4) {
      return totalDmg / 24
    } else if (tickNumber <= 8) {
      return totalDmg / 12
    } else {
      return totalDmg / 8
    }
  }

  updateScoreInfo(scoreInfo: ScoreInfo) {
    // XXX: Very hacky, but update the scoreInfo on the equipment to keep
    // a record we can reference elsewhere without needing to reprocess it
    scoreInfo.targetType = this.target.options.type
    scoreInfo.castDmgEV = this.expectedDmgValuePerCast()
    scoreInfo.castLag = this.options.castLag
    scoreInfo.spellHitWeight = this.spellHitWeight
    scoreInfo.spellCritWeight = this.spellCritWeight
    scoreInfo.spellPenetrationWeight = this.spellPenetrationWeight
    scoreInfo.spellHaste = this.character.getEffectiveSpellHaste()
    scoreInfo.spellBaseCastTime = this.castTime
    scoreInfo.spellEffectiveCastTime = this.effectiveCastTime
    scoreInfo.spellCastTimeWithoutHaste = this.effectiveCastTimeWithoutHaste
    scoreInfo.spellName = this.spell.name
    scoreInfo.spellPower = this.effectiveSpellPower(this.magicSchoolForSpell)
    scoreInfo.spellPowerToDmgRatio = this.spellPowerToDmgRatio
    scoreInfo.spellCoefficient = this.spell.coefficient.direct
    scoreInfo.spellCrit = this.character.getEffectiveSpellCrit(this.talentSchoolForSpell, this.spell.name)
    scoreInfo.effectiveDmgMultiplier = this.effectiveDmgMultiplier(this.spell.magicSchool)
    scoreInfo.magicSchool = this.magicSchoolForSpell

    scoreInfo.maxItemLevel = this.options.maxItemlevel
    scoreInfo.itemType = this.options.itemType
    scoreInfo.pvpItems = this.options.pvpItems
    scoreInfo.encLen = this.options.encLen
    scoreInfo.partialHasteCasts = this.options.partialHasteCasts
    scoreInfo.targetType = this.options.target.type
  }

  _getTrinketDisplayValues(trinket: Item): TrinketDisplayValues[] {
    let displayValues = [] as TrinketDisplayValues[]

    const values = trinketDmgValues(trinket.itemJSON, {
      encLen: this.options.encLen,
      magicSchool: this.magicSchoolForSpell,
      targetType: this.target.options.type,
      castDmgEV: this.expectedDmgValuePerCast(),
      castLag: this.options.castLag,
      spellHaste: this.character.getEffectiveSpellHaste(),
      spellBaseCastTime: this.castTime,
      spellEffectiveCastTime: this.effectiveCastTime,
      spellPower: this.effectiveSpellPower(this.magicSchoolForSpell),
      spellPowerToDmgRatio: this.spellPowerToDmgRatio,
      spellCoefficient: this.spell.coefficient.direct
    })
    if (values.passive) {
      displayValues.push({
        name: values.name,
        type: 'passive',
        avgSPPerCast: values.passive.avgSPPerCast,
        encounterDPS: values.passive.encounterDPS,
        totalCasts: values.passive.totalCasts,
        totalDmg: values.passive.totalDmg,
        totalSP: values.passive.totalSP,
        uptime: values.passive.uptime
      })
    }
    if (values.onUse) {
      displayValues.push({
        name: values.name,
        type: 'on use',
        avgSPPerBuffedCast: values.onUse.avgSPPerBuffedCast,
        buffedCasts: values.onUse.buffedCasts,
        uptimeDPS: values.onUse.uptimeDPS,
        avgSPPerCast: values.onUse.avgSPPerCast,
        encounterDPS: values.onUse.encounterDPS,
        totalCasts: values.onUse.totalCasts,
        totalDmg: values.onUse.totalDmg,
        totalSP: values.onUse.totalSP,
        uptime: values.onUse.uptime
      })
    }
    return displayValues
  }

  effectiveDotTickDmg(baseDmg: number, coefficient: number, spellPower: number, magicSchool: MagicSchool) {
    let tickDmg = baseDmg + coefficient * spellPower

    // https://github.com/slowtorta/turtlewow-bug-tracker/issues/4271#issuecomment-1845256294
    // dots get truncated to the nearest integer per tick
    return Math.floor(tickDmg * this.effectiveDmgMultiplier(magicSchool))
  }

  _dmgData = (dmgFn: Function, min: number, max: number, avg: number) => {
    const minDmg = dmgFn(min)
    const maxDmg = dmgFn(max)
    const avgDmg = dmgFn(avg)

    return {
      min: minDmg,
      max: maxDmg,
      avg: avgDmg,
      text: `${avgDmg.toFixed(0)} (${minDmg.toFixed(0)} - ${maxDmg.toFixed(0)})`
    }
  }
}
