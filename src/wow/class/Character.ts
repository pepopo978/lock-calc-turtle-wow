import Equipment from './Equipment'

import PlayableRace from '../enum/PlayableRace'
import OptionsCharacter from '../interface/OptionsCharacter'
import { BASE_SPELL_CRIT_LOCK, SPELL_CRIT_CAP, SPELL_HASTE_CAP, SPELL_HIT_CAP } from '../module/Constants'
import CharacterBuffs from './CharacterBuffs'
import TalentSchool from '@/wow/enum/TalentSchool'
import MagicSchool from '@/wow/enum/MagicSchool'

/**
 * Stores character attributes, Talents, Gear, and Buffs
 */
export default class Character {
  options: OptionsCharacter
  equipment: Equipment
  buffs: CharacterBuffs

  constructor(options: OptionsCharacter, equipment: Equipment) {
    this.options = options
    this.equipment = equipment
    this.buffs = new CharacterBuffs(options)
  }

  get level(): number {
    return this.options.level
  }

  /**
   * TODO: https://classicwow.live/guides/46/basic-stats-sheet
   */

  get health(): number {
    return 10 * this.stamina + this.equipment.health
  }

  get mana(): number {
    return 964 + 15 * this.intellect
  }

  get raceStamina(): number {
    switch (this.options.race) {
      case PlayableRace.Human:
        return 20 + 25 // not sure if this is accurate
      case PlayableRace.Orc:
        return 23 + 25 // not sure if this is accurate
      case PlayableRace.Dwarf:
        return 25 + 25 // not sure if this is accurate
      case PlayableRace.NightElf:
        return 20 + 25 // not sure if this is accurate
      case PlayableRace.Undead:
        return 21 + 25 // not sure if this is accurate
      case PlayableRace.Tauren:
        return 25 + 25 // not sure if this is accurate
      case PlayableRace.Gnome:
        return 19 + 25 // not sure if this is accurate
      case PlayableRace.Troll:
        return 22 + 25 // not sure if this is accurate
      case PlayableRace.Goblin:
        return 22 + 25 // not sure if this is accurate
      case PlayableRace.HighElf:
        return 19 + 25 // not sure if this is accurate
      default:
        return 0
    }
  }

  get armor(): number {
    return this.equipment.armor + this.buffs.demonArmorArmorBonus
  }

  get stamina(): number {
    return (
      (this.raceStamina + this.equipment.stamina + this.buffs.GiftOfTheWildRk2AttributeBonus) *
      this.buffs.spiritOfZandalarBonus *
      this.buffs.blessingOfKingsStatsBonus
    )
  }

  get raceIntellect(): number {
    switch (this.options.race) {
      case PlayableRace.Human:
        return 20 + 118 // not sure if this is accurate
      case PlayableRace.Orc:
        return 17 + 118 // not sure if this is accurate
      case PlayableRace.Dwarf:
        return 19 + 118 // not sure if this is accurate
      case PlayableRace.NightElf:
        return 20 + 118 // not sure if this is accurate
      case PlayableRace.Undead:
        return 18 + 118 // not sure if this is accurate
      case PlayableRace.Tauren:
        return 16 + 118 // not sure if this is accurate
      case PlayableRace.Gnome:
        return 23 + 118 // not sure if this is accurate
      case PlayableRace.Troll:
        return 16 + 118 // not sure if this is accurate
      case PlayableRace.Goblin:
        return 16 + 118 // not sure if this is accurate
      case PlayableRace.HighElf:
        return 23 + 118 // not sure if this is accurate
      default:
        return 0
    }
  }

  get intellect(): number {
    const gnomeMultiplier = this.options.race === PlayableRace.Gnome ? 1.05 : 1
    return (
      (this.raceIntellect +
        this.equipment.intellect +
        this.buffs.arcaneBrillianceIntBonus +
        this.buffs.GiftOfTheWildRk2AttributeBonus +
        this.buffs.songflowerSerenadeAttributeBonus +
        this.buffs.cerebralCortexCompoundIntBonus +
        this.buffs.medivhsMerlotBlueIntBonus +
        this.buffs.runnTumTuberSurpriseIntBonus) *
      this.buffs.spiritOfZandalarBonus *
      this.buffs.blessingOfKingsStatsBonus *
      gnomeMultiplier
    )
  }

  get raceSpirit(): number {
    switch (this.options.race) {
      case PlayableRace.Human:
        return 20 + 100 // not sure if this is accurate
      case PlayableRace.Orc:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.Dwarf:
        return 20 + 100 // not sure if this is accurate
      case PlayableRace.NightElf:
        return 25 + 100 // not sure if this is accurate
      case PlayableRace.Undead:
        return 23 + 100 // not sure if this is accurate
      case PlayableRace.Tauren:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.Gnome:
        return 20 + 100 // not sure if this is accurate
      case PlayableRace.Troll:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.Goblin:
        return 22 + 100 // not sure if this is accurate
      case PlayableRace.HighElf:
        return 20 + 100 // not sure if this is accurate
      default:
        return 0
    }
  }

  get spirit(): number {
    return (
      this.raceSpirit +
      this.equipment.spirit +
      this.buffs.GiftOfTheWildRk2AttributeBonus *
      this.buffs.spiritOfZandalarBonus *
      this.buffs.blessingOfKingsStatsBonus
    )
  }

  get mp5(): number {
    return (
      this.equipment.mp5 +
      this.buffs.blessingOfWisdomMp5Bonus +
      this.buffs.manaSpringTotemMp5Bonus +
      this.buffs.druidAtieshMp5Bonus
    )
  }

  get hp5(): number {
    return this.equipment.hp5 + this.buffs.demonArmorHp5Bonus
  }

  get percentManaRegenWhileCasting(): number {
    return (
      this.equipment.percentManaRegenWhileCasting +
      this.buffs.emeraldBlessingRegenWhileCastingPercent
    )
  }

  get fireResistance(): number {
    return this.equipment.fireResistance
  }

  get frostResistance(): number {
    return this.equipment.frostResistance
  }

  get arcaneResistance(): number {
    return this.equipment.arcaneResistance
  }

  get natureResistance(): number {
    return this.equipment.natureResistance
  }

  get shadowResistance(): number {
    return this.equipment.shadowResistance
  }

  get manaPerTickNotCasting(): number {
    const fromBase = (15 * this.level) / 60
    const fromSpirit = this.spirit / 5
    const fromMp5 = this.mp5 ? (this.mp5 / 5) * 2 : 0

    return fromBase + fromSpirit + fromMp5
  }

  get manaPerTickCastingNoMp5(): number {
    const fromBase = (15 * this.level) / 60
    const fromSpirit = this.spirit / 5

    return Math.round((this.percentManaRegenWhileCasting / 100) * (fromBase + fromSpirit))
  }

  get manaPerTickCasting(): number {
    const fromMp5 = this.mp5 ? (this.mp5 / 5) * 2 : 0

    return fromMp5 + this.manaPerTickCastingNoMp5
  }

  get manaPerTickInnervate(): number {
    return this.manaPerTickNotCasting * 4
  }

  get manaPerInnervate(): number {
    return this.manaPerTickInnervate * 10
  }

  get spellPower(): number {
    return this.equipment.spellPower
  }

  get arcaneSpellPower(): number {
    return this.equipment.arcaneSpellPower
  }

  get frostSpellPower(): number {
    return this.equipment.frostSpellPower
  }

  get fireSpellPower(): number {
    return this.equipment.fireSpellPower
  }

  get natureSpellPower(): number {
    return this.equipment.natureSpellPower
  }

  get shadowDamage(): number {
    return this.equipment.shadowSpellPower
  }

  get spellCritFromIntellect(): number {
    return this.intellect / 59.5
  }

  get spellCritFromEquipment(): number {
    return this.equipment.spellCrit
  }

  get spellVampFromEquipment(): number {
    return this.equipment.spellVamp
  }

  get suppressionHitChance(): number {
    return this.options.talents.affliction.suppression * 2
  }

  get baneShadowboltCastTimeReduction(): number {
    return this.options.talents.destruction.bane * 0.1
  }

  get baneImmolateCastTimeReduction(): number {
    return this.options.talents.destruction.bane * 0.4
  }

  get impFireboltCastTimeReduction(): number {
    return this.options.talents.destruction.improvedFirebolt * 0.5
  }

  get improvedCorruptionTimeReduction(): number {
    return this.options.talents.affliction.improvedCorruption * 0.4
  }

  get devastationCritChance(): number {
    return this.options.talents.destruction.devastation
  }

  get ruinCritBonusDamage(): number {
    return this.options.talents.destruction.ruin * 0.5
  }

  get improvedImmolateDamageBonus(): number {
    return 1 + this.options.talents.destruction.improvedImmolate * 0.05
  }

  emberstormDamageBonus(spellSchool: MagicSchool): number {
    return spellSchool === MagicSchool.Fire ? 1 + this.options.talents.destruction.emberstorm * 0.02 : 1
  }

  // TALENTS
  getImprovedShadowBoltShadowDmgBonus(magicSchool: MagicSchool): number {
    if (magicSchool !== MagicSchool.Shadow) {
      return 1
    }

    return 1 + this.options.talents.destruction.improvedShadowBolt * 0.04
  }

  getDemonicSacrificeShadowDmgBonus(magicSchool: MagicSchool): number {
    if (magicSchool !== MagicSchool.Shadow) {
      return 1
    }

    return 1 + this.options.talents.demonology.demonicSacrifice * 0.15
  }

  getShadowMasteryShadowDmgBonus(magicSchool: MagicSchool): number {
    if (magicSchool !== MagicSchool.Shadow) {
      return 1
    }

    return 1 + this.options.talents.affliction.shadowMastery * 0.02
  }

  getTalentSpellCrit(talentSchool: TalentSchool, _spellName: string): number {
    return talentSchool === TalentSchool.Destruction ? this.devastationCritChance : 0
  }

  getSpellCritUnbuffed(talentSchool: TalentSchool, spellName: string): number {
    return (
      BASE_SPELL_CRIT_LOCK +
      this.spellCritFromIntellect +
      this.spellCritFromEquipment +
      this.getTalentSpellCrit(talentSchool, spellName)
    )
  }

  getSpellCrit(talentSchool: TalentSchool, spellName: string): number {
    return (
      this.getSpellCritUnbuffed(talentSchool, spellName) +
      this.buffs.rallyingCryOfTheDragonSlayerSpellCritBonus +
      this.buffs.dreamshardElixirCritBonus +
      this.buffs.moonkinAuraBonus +
      this.buffs.slipkiksSavvyCritChanceBonus +
      this.buffs.songflowerSerenadeSpellCritBonus +
      this.buffs.brilliantWizardOilSpellCritBonus +
      this.buffs.mageAtieshSpellCritBonus
    )
  }

  getEffectiveSpellCrit(talentSchool: TalentSchool, spellName: string): number {
    return Math.min(this.getSpellCrit(talentSchool, spellName), SPELL_CRIT_CAP)
  }

  getSpellHit(talentSchool: TalentSchool): number {
    const spellSchoolTalentHit = talentSchool === TalentSchool.Affliction ? this.suppressionHitChance : 0

    return this.equipment.spellHit + this.buffs.emeraldBlessingHitBonus + spellSchoolTalentHit
  }

  /**
   * TODO: Return total spell hit rating (equipment + talents + buffs)
   */
  getEffectiveSpellHit(talentSchool: TalentSchool): number {
    return Math.min(this.getSpellHit(talentSchool), SPELL_HIT_CAP)
  }

  getEffectiveSpellHaste(): number {
    const gearSpellHaste = this.equipment.spellHaste
    return Math.min(
      gearSpellHaste + this.buffs.telAbimMedleyHasteBonus + this.buffs.graceOfTheSunwellHasteBonus,
      SPELL_HASTE_CAP
    )
  }

  getSpellVamp(_talentSchool: TalentSchool): number {
    return this.spellVampFromEquipment
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          try {
            const val = (this as any)[key]
            jsonObj[key] = val
          } catch (error) {
            console.error(`Error calling getter ${key}`, error)
          }
        }
      })

    return jsonObj
  }
}
