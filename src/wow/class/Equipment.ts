import Item from './Item'
import Spell from './Spell'
import Tools from '../module/Tools'
import Query from '../module/Query'

import ItemSlot from '../enum/ItemSlot'
import SortOrder from '../enum/SortOrder'

import OptionsInterface from '../interface/Options'
import ItemJSON, { createDummyItemJSON } from '../interface/ItemJSON'
import EnchantJSON from '../interface/EnchantJSON'
import ScoreInfo from '../interface/ScoreInfo'

export default class Equipment {
  options: OptionsInterface
  scoreInfo: ScoreInfo
  items: {
    head?: Item
    neck?: Item
    shoulder?: Item
    chest?: Item
    waist?: Item
    legs?: Item
    feet?: Item
    wrist?: Item
    hands?: Item
    finger?: Item
    finger2?: Item
    trinket?: Item
    trinket2?: Item
    ranged?: Item
    back?: Item
    onehand?: Item
    twohand?: Item
    mainhand?: Item
    offhand?: Item
  }

  /* TODO: can I make it so the constructor could take list of item ids or something instead? */
  constructor(
    options: OptionsInterface,
    spellHitWeight?: number,
    spellCritWeight?: number,
    spellPenetrationWeight?: number,
    spellCastTime?: number,
    spellCrit?: number
  ) {
    this.options = options
    this.scoreInfo = this.scoreInfoFromOptions(
      options,
      spellHitWeight,
      spellCritWeight,
      spellPenetrationWeight,
      spellCastTime,
      spellCrit
    )

    this.items = {
      head: undefined,
      hands: undefined,
      neck: undefined,
      waist: undefined,
      shoulder: undefined,
      legs: undefined,
      back: undefined,
      feet: undefined,
      chest: undefined,
      finger: undefined,
      wrist: undefined,
      finger2: undefined,
      mainhand: undefined,
      offhand: undefined,
      ranged: undefined,
      trinket: undefined,
      trinket2: undefined
    }
  }

  get spellPower(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    const itemSpellDamage = Object.values(items)
      .map(item => (item && item.matchesTargetType(this.options.target.type) ? item.spellPower : 0))
      .reduce((prev, next) => prev + next)

    return itemSpellDamage
  }

  get arcaneSpellPower(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item && item.matchesTargetType(this.options.target.type) ? item.arcaneDamage : 0))
      .reduce((prev, next) => prev + next)
  }

  get fireSpellPower(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item && item.matchesTargetType(this.options.target.type) ? item.fireDamage : 0))
      .reduce((prev, next) => prev + next)
  }

  // get frost damage
  get frostSpellPower(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item && item.matchesTargetType(this.options.target.type) ? item.frostDamage : 0))
      .reduce((prev, next) => prev + next)
  }

  get natureSpellPower(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item && item.matchesTargetType(this.options.target.type) ? item.natureDamage : 0))
      .reduce((prev, next) => prev + next)
  }

  get shadowSpellPower(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item && item.matchesTargetType(this.options.target.type) ? item.shadowDamage : 0))
      .reduce((prev, next) => prev + next)
  }

  get spellHit(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.spellHit : 0))
      .reduce((prev, next) => prev + next)
  }

  get spellCrit(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.spellCrit : 0))
      .reduce((prev, next) => prev + next)
  }

  get spellHaste(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    const hasteValues = Object.values(items).map(item => (item ? item.spellHaste : 0))

    // sort by highest haste
    hasteValues.sort((a, b) => b - a)

    let totalHasteScaling = 1
    // loop through haste values and apply multiplicative haste
    for (let i = 0; i < hasteValues.length; i++) {
      totalHasteScaling = totalHasteScaling * (1 + hasteValues[i] / 100)
    }

    // convert back to percentage
    return (totalHasteScaling - 1) * 100
  }

  get spellVamp(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? (item.spellVamp ? item.spellVamp : 0) : 0))
      .reduce((prev, next) => prev + next)
  }

  get spellPenetration(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.spellPenetration : 0))
      .reduce((prev, next) => prev + next)
  }

  get armor(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.armor : 0))
      .reduce((prev, next) => prev + next)
  }

  get intellect(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.intellect : 0))
      .reduce((prev, next) => prev + next)
  }

  get stamina(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.stamina : 0))
      .reduce((prev, next) => prev + next)
  }

  get health(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.health : 0))
      .reduce((prev, next) => prev + next)
  }

  get spirit(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.spirit : 0))
      .reduce((prev, next) => prev + next)
  }

  get mp5(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.mp5 : 0))
      .reduce((prev, next) => prev + next)
  }

  get hp5(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.hp5 : 0))
      .reduce((prev, next) => prev + next)
  }

  get percentManaRegenWhileCasting(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item =>
        item && item.itemJSON.effects.manaRegenWhileCasting ? item.itemJSON.effects.manaRegenWhileCasting : 0
      )
      .reduce((prev, next) => prev + next)
  }

  get fireResistance(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.itemJSON.fireRes : 0))
      .reduce((prev, next) => prev + next)
  }

  get frostResistance(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.itemJSON.frostRes : 0))
      .reduce((prev, next) => prev + next)
  }

  get arcaneResistance(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.itemJSON.arcaneRes : 0))
      .reduce((prev, next) => prev + next)
  }

  get natureResistance(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.itemJSON.natureRes : 0))
      .reduce((prev, next) => prev + next)
  }

  get shadowResistance(): number {
    const items = Object.values(this.items).concat(this.getItemSetVirtualItems())
    return Object.values(items)
      .map(item => (item ? item.itemJSON.shadowRes : 0))
      .reduce((prev, next) => prev + next)
  }

  _bisOrLocked(slot: ItemSlot) {
    if (this.items) {
      const item = this.getItemForSlot(slot)
      if (item && item.locked) {
        return item
      }
    }
    return this.getBestInSlotItemWithEnchant(slot, this.scoreInfo)
  }

  _bisOrLockedWithItemAndEnchant(slot: ItemSlot, itemJSON?: ItemJSON, enchantJSON?: EnchantJSON) {
    if (this.items) {
      const item = this.getItemForSlot(slot)
      if (item && item.locked) {
        return item
      }
    }
    if (!itemJSON) {
      return undefined
    }
    return new Item(slot, itemJSON, enchantJSON)
  }

  _findBisItems() {
    const bisTrinkets = this.getBestInSlotTrinkets(this.scoreInfo)
    const bisRings = this.getBestInSlotRings(this.scoreInfo)
    const bisWeaponCombo = this.getBestInSlotWeaponCombo(this.scoreInfo)
    const bisChestLegsFeet = this.getBestInSlotChestLegsFeet(this.scoreInfo)

    const mainhand = bisWeaponCombo.mainHand
    const offhand = bisWeaponCombo.offHand
    const enchant = bisWeaponCombo.enchant

    return {
      head: this._bisOrLocked(ItemSlot.Head),
      hands: this._bisOrLocked(ItemSlot.Hands),
      neck: this._bisOrLocked(ItemSlot.Neck),
      waist: this._bisOrLocked(ItemSlot.Waist),
      shoulder: this._bisOrLocked(ItemSlot.Shoulder),
      legs: this._bisOrLockedWithItemAndEnchant(ItemSlot.Legs, bisChestLegsFeet.legs, bisChestLegsFeet.legsEnchant),
      back: this._bisOrLocked(ItemSlot.Back),
      feet: this._bisOrLockedWithItemAndEnchant(ItemSlot.Feet, bisChestLegsFeet.feet, bisChestLegsFeet.feetEnchant),
      chest: this._bisOrLockedWithItemAndEnchant(ItemSlot.Chest, bisChestLegsFeet.chest, bisChestLegsFeet.chestEnchant),
      finger: this._bisOrLockedWithItemAndEnchant(ItemSlot.Finger, bisRings ? bisRings.finger : undefined),
      wrist: this._bisOrLocked(ItemSlot.Wrist),
      finger2: this._bisOrLockedWithItemAndEnchant(ItemSlot.Finger2, bisRings ? bisRings.finger2 : undefined),
      mainhand: this._bisOrLockedWithItemAndEnchant(ItemSlot.Mainhand, mainhand, enchant),
      offhand: this._bisOrLockedWithItemAndEnchant(ItemSlot.Offhand, offhand, enchant),
      ranged: this._bisOrLocked(ItemSlot.Ranged),
      trinket: this._bisOrLockedWithItemAndEnchant(ItemSlot.Trinket, bisTrinkets ? bisTrinkets.trinket : undefined),
      trinket2: this._bisOrLockedWithItemAndEnchant(ItemSlot.Trinket2, bisTrinkets ? bisTrinkets.trinket2 : undefined)
    }
  }

  // once other equipment is picked we can use the final spell power to score trinkets that depend on it
  updateTrinkets() {
    const bisTrinkets = this.getBestInSlotTrinkets(this.scoreInfo)
    this.items.trinket = this._bisOrLockedWithItemAndEnchant(
      ItemSlot.Trinket,
      bisTrinkets ? bisTrinkets.trinket : undefined
    )
    this.items.trinket2 = this._bisOrLockedWithItemAndEnchant(
      ItemSlot.Trinket2,
      bisTrinkets ? bisTrinkets.trinket2 : undefined
    )
  }

  fillItems() {
    this.items = this._findBisItems()

    // fill in any unlocked enchants
    for (const slot in ItemSlot) {
      const item = this.getItemForSlot(slot as ItemSlot)
      if (item && !item.enchantLocked) {
        item.enchantJSON = this.getBestInSlotEnchant(slot as ItemSlot, this.scoreInfo)
      }
    }
  }

  getWeightedItemsBySlot(slot: ItemSlot, scoreInfo: ScoreInfo) {
    const results = Query.Items({
      cloneResults: true,
      slot: slot,
      maxItemLevel: scoreInfo.maxItemLevel,
      itemType: scoreInfo.itemType,
      pvp: scoreInfo.pvpItems
    })

    /* score items */
    if (results && results.length > 0) {
      for (let i = 0; i < results.length; i++) {
        const result = results[i]

        result.score = Item.scoreItem(result, scoreInfo)

        if (result.setName !== '' && this.items) {
          result.setBonusScore = Item.scoreItemSetBonus(result, this, scoreInfo)
          result.score += result.setBonusScore
          result.score = Math.round(result.score * 100) / 100 // round to 2 decimal
        }
      }

      results.sort(scoreInfo.sortOrder === SortOrder.Descending ? Item.sortScoreDes : Item.sortScoreAsc)
    }
    return results
  }

  getWeightedEnchantsBySlot(slot: ItemSlot, scoreInfo: ScoreInfo) {
    const results = Query.Enchants({
      cloneResults: true,
      slot: slot
    })

    if (results) {
      for (const i in results) {
        results[i].score = Item.scoreEnchant(results[i], scoreInfo)
      }
      results.sort(scoreInfo.sortOrder === SortOrder.Descending ? Item.sortScoreDes : Item.sortScoreAsc)
    }
    return results
  }

  scoreInfoFromOptions(
    options: OptionsInterface,
    spellHitWeight?: number,
    spellCritWeight?: number,
    _spellPenetrationWeight?: number,
    spellCastTime?: number,
    spellCrit?: number
  ): ScoreInfo {
    const myOptions: OptionsInterface = Tools.CloneObject(options)
    const spell = new Spell(myOptions.spellName)
    const mySpellHitWeight = spellHitWeight !== undefined ? spellHitWeight : 15
    const mySpellCritWeight = spellCritWeight !== undefined ? spellCritWeight : 10

    // TODO: recommending spell pen gear is disabled. it suffers the same problem as spell hit cap
    const mySpellPenetrationWeight = 0
    // const mySpellPenetrationWeight = spellPenetrationWeight !== undefined ? spellPenetrationWeight : 0
    const mySpellCastTime = spellCastTime !== undefined ? spellCastTime : spell.castTime
    const mySpellCrit = spellCrit !== undefined ? spellCrit : 30

    return {
      encLen: myOptions.encLen,
      partialHasteCasts: myOptions.partialHasteCasts,
      maxItemLevel: myOptions.maxItemlevel,
      itemType: myOptions.itemType,
      pvpItems: myOptions.pvpItems,
      magicSchool: spell.magicSchool,
      targetType: myOptions.target.type,
      castLag: myOptions.castLag,
      spellName: myOptions.spellName,
      spellPower: 0,
      effectiveDmgMultiplier: 1,
      spellHitWeight: mySpellHitWeight,
      spellCritWeight: mySpellCritWeight,
      spellPenetrationWeight: mySpellPenetrationWeight,
      spellHaste: 0,
      spellBaseCastTime: spell.castTime,
      spellEffectiveCastTime: mySpellCastTime,
      spellCastTimeWithoutHaste: mySpellCastTime,
      spellCrit: mySpellCrit,
      spellCoefficient: spell.coefficient.direct,
      spellPowerToDmgRatio: spell.coefficient.direct, // default to coefficient until we can calculate hit/crit using equip
      castDmgEV: spell.avgDmg, // default to initial cast dmg
      slot: myOptions.itemSearchSlot ? myOptions.itemSearchSlot : ItemSlot.Head, // default to dummy slot, search won't be shown if scoreInfoSlot undefined
      sortOrder: SortOrder.Descending
    }
  }

  optimalEnchantsForSlot(slot: ItemSlot) {
    return this.getWeightedEnchantsBySlot(slot, this.scoreInfo)
  }

  optimalItemsForSlot(slot: ItemSlot, scoreInfo: ScoreInfo) {
    return this.getWeightedItemsBySlot(slot, scoreInfo)
  }

  getBestInSlotItem(slot: ItemSlot, scoreInfo: ScoreInfo) {
    const results = this.getWeightedItemsBySlot(slot, scoreInfo)
    return results ? results[0] : undefined
  }

  getBestInSlotEnchant(slot: ItemSlot, scoreInfo: ScoreInfo) {
    const results = this.getWeightedEnchantsBySlot(slot, scoreInfo)
    return results ? results[0] : undefined
  }

  getBestInSlotItemWithEnchant(slot: ItemSlot, scoreInfo: ScoreInfo) {
    const item = this.getBestInSlotItem(slot, scoreInfo)
    const enchant = this.getBestInSlotEnchant(slot, scoreInfo)
    return item ? new Item(slot, item, enchant) : undefined
  }

  getBestInSlotChestLegsFeet(scoreInfo: ScoreInfo) {
    const chest: ItemJSON | undefined = this.getBestInSlotItem(ItemSlot.Chest, scoreInfo)
    const legs: ItemJSON | undefined = this.getBestInSlotItem(ItemSlot.Legs, scoreInfo)
    const feet: ItemJSON | undefined = this.getBestInSlotItem(ItemSlot.Feet, scoreInfo)

    return {
      chest: chest,
      chestEnchant: this.getBestInSlotEnchant(ItemSlot.Chest, scoreInfo),
      legs: legs,
      legsEnchant: this.getBestInSlotEnchant(ItemSlot.Legs, scoreInfo),
      feet: feet,
      feetEnchant: this.getBestInSlotEnchant(ItemSlot.Feet, scoreInfo)
    }
  }

  getBestInSlotTrinkets(scoreInfo: ScoreInfo) {
    const result = this.getWeightedItemsBySlot(ItemSlot.Trinket, scoreInfo)
    const result2 = this.getWeightedItemsBySlot(ItemSlot.Trinket2, scoreInfo)
    if (!result || !result2) {
      return undefined
    }

    const t1 = 0
    let t2 = 0

    if (result[t1].unique && result[t1].name === result2[t2].name) {
      t2++
    }

    const trinket1 = result[t1]
    const trinket2 = result2[t2]

    return {
      trinket: trinket1,
      trinket2: trinket2
    }
  }

  getBestInSlotRings(scoreInfo: ScoreInfo) {
    const result = this.getWeightedItemsBySlot(ItemSlot.Finger, scoreInfo)
    const result2 = this.getWeightedItemsBySlot(ItemSlot.Finger2, scoreInfo)
    if (!result || !result2) {
      return undefined
    }

    const ring1: ItemJSON = result[0]
    let ring2: ItemJSON = result2[0]
    if (result[0].unique && result[0].name === result2[0].name) {
      ring2 = result2[1]
    }

    return {
      finger: ring1,
      finger2: ring2
    }
  }

  getBestInSlotWeaponCombo(scoreInfo: ScoreInfo) {
    const twohand = this.getBestInSlotItem(ItemSlot.Twohand, scoreInfo)
    const onehand = this.getBestInSlotItem(ItemSlot.Onehand, scoreInfo)
    const offhand = this.getBestInSlotItem(ItemSlot.Offhand, scoreInfo)
    const enchant = this.getBestInSlotEnchant(ItemSlot.Mainhand, scoreInfo)

    const onehandscore = onehand && onehand.score ? onehand.score : 0
    const offhandscore = offhand && offhand.score ? offhand.score : 0
    const twohandscore = twohand && twohand.score ? twohand.score : 0

    if (twohandscore > onehandscore + offhandscore) {
      return {
        mainHand: twohand,
        enchant: enchant
      }
    }

    const mainhand = onehand

    return {
      mainHand: mainhand,
      offHand: offhand,
      enchant: enchant
    }
  }

  getItemForSlot(slot: ItemSlot): Item | undefined {
    switch (slot) {
      case ItemSlot.Head:
        return this.items.head
      case ItemSlot.Neck:
        return this.items.neck
      case ItemSlot.Shoulder:
        return this.items.shoulder
      case ItemSlot.Back:
        return this.items.back
      case ItemSlot.Chest:
        return this.items.chest
      case ItemSlot.Wrist:
        return this.items.wrist
      case ItemSlot.Hands:
        return this.items.hands
      case ItemSlot.Waist:
        return this.items.waist
      case ItemSlot.Legs:
        return this.items.legs
      case ItemSlot.Feet:
        return this.items.feet
      case ItemSlot.Finger:
        return this.items.finger
      case ItemSlot.Finger2:
        return this.items.finger2
      case ItemSlot.Trinket:
        return this.items.trinket
      case ItemSlot.Trinket2:
        return this.items.trinket2
      case ItemSlot.Onehand:
      case ItemSlot.Twohand:
      case ItemSlot.Mainhand:
        return this.items.mainhand
      case ItemSlot.Offhand:
        return this.items.offhand
      case ItemSlot.Ranged:
        return this.items.ranged
      default:
        console.log(`Unable to get item for slot |${slot}|`)
    }
  }

  _setItemForSlot(slot: ItemSlot, item: Item | undefined) {
    switch (slot) {
      case ItemSlot.Head:
        this.items.head = item
        break
      case ItemSlot.Neck:
        this.items.neck = item
        break
      case ItemSlot.Shoulder:
        this.items.shoulder = item
        break
      case ItemSlot.Back:
        this.items.back = item
        break
      case ItemSlot.Chest:
        this.items.chest = item
        break
      case ItemSlot.Wrist:
        this.items.wrist = item
        break
      case ItemSlot.Hands:
        this.items.hands = item
        break
      case ItemSlot.Waist:
        this.items.waist = item
        break
      case ItemSlot.Legs:
        this.items.legs = item
        break
      case ItemSlot.Feet:
        this.items.feet = item
        break
      case ItemSlot.Finger:
        this.items.finger = item
        break
      case ItemSlot.Finger2:
        this.items.finger2 = item
        break
      case ItemSlot.Trinket:
        this.items.trinket = item
        break
      case ItemSlot.Trinket2:
        this.items.trinket2 = item
        break
      case ItemSlot.Twohand:
      case ItemSlot.Mainhand:
      case ItemSlot.Onehand:
        this.items.mainhand = item
        break
      case ItemSlot.Offhand:
        this.items.offhand = item
        break
      case ItemSlot.Ranged:
        this.items.ranged = item
        break
      default:
        console.log(`Unable to set item for slot ${slot}`)
    }
  }

  isLocked(slot: ItemSlot): boolean {
    const item = this.getItemForSlot(slot)
    return item ? item.locked : false
  }

  isEquipped(slot: ItemSlot): boolean {
    const item = this.getItemForSlot(slot)
    return item ? true : false
  }

  setItem(slot: ItemSlot, item: Item | undefined) {
    this._setItemForSlot(slot, item)
  }

  setItemFromId(slot: ItemSlot, itemId: number) {
    // query the item
    const item = Query.Item({ id: itemId })
    if (item) {
      // score item
      item.score = Item.scoreItem(item, this.scoreInfo)
      this._setItemForSlot(slot, new Item(slot, item))
    }
  }

  setEnchant(slot: ItemSlot, enchant: EnchantJSON) {
    const item = this.getItemForSlot(slot)
    if (item) {
      item.enchantJSON = enchant
    }
  }

  setEnchantFromId(slot: ItemSlot, enchantId: number) {
    // query the item
    const enchantJSON = Query.Enchant({ id: enchantId })
    if (enchantJSON) {
      this.setEnchant(slot, enchantJSON)
    }
  }

  lockItem(slot: ItemSlot) {
    const item = this.getItemForSlot(slot)
    if (item) {
      item.locked = true
    }
  }

  lockEnchant(slot: ItemSlot) {
    const item = this.getItemForSlot(slot)
    if (item) {
      item.enchantLocked = true
    }
  }

  unequipItem(slot: ItemSlot) {
    // check if locked first
    if (!this.isLocked(slot)) {
      this._setItemForSlot(slot, undefined)
    }
  }

  unequipEnchant(slot: ItemSlot) {
    const item = this.getItemForSlot(slot)
    if (item && !item.enchantLocked) {
      item.enchantJSON = undefined
    }
  }

  unlockItem(slot: ItemSlot) {
    const item = this.getItemForSlot(slot)
    if (item) {
      item.locked = false
    }
  }

  unlockEnchant(slot: ItemSlot) {
    const item = this.getItemForSlot(slot)
    if (item) {
      item.enchantLocked = false
    }
  }

  lockItems() {
    for (const slot in ItemSlot) {
      this.lockItem(slot as ItemSlot)
    }
  }

  unlockItems() {
    for (const slot in ItemSlot) {
      this.unlockItem(slot as ItemSlot)
    }
  }

  unequipItems() {
    for (const slot in ItemSlot) {
      this.unequipItem(slot as ItemSlot)
    }
  }

  lockEnchants() {
    for (const slot in ItemSlot) {
      this.lockEnchant(slot as ItemSlot)
    }
  }

  unequipEnchants() {
    for (const slot in ItemSlot) {
      this.unequipEnchant(slot as ItemSlot)
    }
  }

  unlockEnchants() {
    for (const slot in ItemSlot) {
      this.unlockEnchant(slot as ItemSlot)
    }
  }

  getItemSetVirtualItems(searchItemJson?: ItemJSON): Item[] {
    // treat set bonuses like a virtual item
    const virtualItems = new Array<Item>()

    const numItemsSeenForSet = new Map<string, number>()
    const seenItemNamesForSet = new Map<string, Array<string>>()

    const items = Object.values(this.items)
    if (searchItemJson) {
      // check if search item already equipped
      let equipped = false
      items.forEach(item => {
        if (item && item.itemJSON.name === searchItemJson.name) {
          equipped = true
        }
      })

      // if not equipped, include it in set bonus calculations
      if (!equipped) {
        // create dummy item with the search item json
        items.push(new Item(ItemSlot.Head, searchItemJson))
      }
    }

    items.forEach(item => {
      if (item && item.isPartOfItemSet) {
        // if search item provided, only include set bonuses for that set
        if (!searchItemJson || item.itemJSON.setName === searchItemJson.setName) {
          const setName = item.itemJSON.setName
          let numItemsSeen = numItemsSeenForSet.get(setName)
          if (numItemsSeen) {
            numItemsSeen += 1
          } else {
            numItemsSeen = 1
          }
          numItemsSeenForSet.set(setName, numItemsSeen)

          let seenItemNames = seenItemNamesForSet.get(setName)
          if (seenItemNames) {
            seenItemNames.push(item.itemJSON.name)
          } else {
            seenItemNames = new Array<string>()
            seenItemNames.push(item.itemJSON.name)
          }
          seenItemNamesForSet.set(setName, seenItemNames)

          // check if we've seen enough items to get a set bonus
          if (item.itemJSON.setBonuses.hasOwnProperty(numItemsSeen)) {
            // create a virtual item for the set bonus
            const virtualItemJson = createDummyItemJSON()

            // only add effects if it matches the target type
            if (item.matchesTargetType(this.options.target.type)) {
              virtualItemJson.effects = item.itemJSON.setBonuses[numItemsSeen] // attach parsed effects
            }

            virtualItemJson.name = `(${numItemsSeen}) ${setName}`
            virtualItemJson.setName = setName
            virtualItemJson.setItems = seenItemNames
            virtualItems.push(new Item(ItemSlot.Head, virtualItemJson))
          }
        }
      }
    })
    return virtualItems
  }
}
