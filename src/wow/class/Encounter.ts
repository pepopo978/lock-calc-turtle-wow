import OptionsInterface from '../interface/Options'
import Cast from './Cast'

/* Encounter is the big top level object for all wow calculations. We want it run exactly once
   whenever a value in Options is changed.

   - Creates the Cast() object where most work is done
   - Generates the item and enchant list when clicking an item/enchant
   - Does the expensive gear optimization
*/
export default class Encounter {
  options: OptionsInterface
  spellCast: Cast

  constructor(options: OptionsInterface) {
    this.options = options
    this.spellCast = new Cast(options)
  }

  get equipment() {
    return this.spellCast.character.equipment
  }
}
