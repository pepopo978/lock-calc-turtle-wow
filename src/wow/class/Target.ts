import Debuff from '../enum/Debuffs'
import MagicSchool from '../enum/MagicSchool'
import OptionsTarget from '../interface/OptionsTarget'

/* XXX: I originally wanted to allow user to select a target from a list. The lack of
 * resistance information on bosses makes this pretty fruitless, so I abandoned it.
interface TargetJSON {
  name: string
  level: number
  class: string
  faction: string
  health: number
  minDmg: number
  maxDmg: number
  attackSpeed: number
  armor: number
  fireResist: number
  natureResist: number
  frostResist: number
  shadowResist: number
  arcaneResist: number
}
*/

export default class Target {
  options: OptionsTarget

  constructor(options: OptionsTarget) {
    this.options = options
  }

  get level(): number {
    return this.options.level
  }

  /* assumes character level 60 */
  get hitChance(): number {
    switch (this.level) {
      case 63:
        return 83
      case 62:
        return 94
      case 61:
        return 95
      case 60:
        return 96
      case 59:
        return 97
      case 58:
        return 98
      default:
        return 99
    }
  }

  get spellResistance(): number {
    return this.options.spellResistance
  }

  get shimmer(): MagicSchool {
    return this.options.shimmer
  }

  get spellVulnerabilityBonus(): number {
    return this.hasDebuff(Debuff.SpellVulnerability) ? 1.15 : 1.0
  }

  /**
   * ...reducing Shadow and Arcane resistances by 75...
   */
  get curseOfShadowResistBonus(): number {
    return this.hasDebuff(Debuff.CurseOfShadow) ? 75 : 0
  }

  /**
   * ...reducing Fire and Frost resistances by 75...
   */
  get curseOfElementsResistBonus(): number {
    return this.hasDebuff(Debuff.CurseOfElements) ? 75 : 0
  }

  /**
   * ...and increasing Fire and Frost damage taken by 10%...
   */
  get curseOfElementsDamageBonus(): number {
    return this.hasDebuff(Debuff.CurseOfElements) ? 1.1 : 1.0
  }

  /**
   * ...and increasing Shadow and Arcane damage taken by 10%...
   */
  get curseOfShadowDamageBonus(): number {
    return this.hasDebuff(Debuff.CurseOfShadow) ? 1.1 : 1.0
  }

  get arcaneSpellResistance(): number {
    return this.options.spellResistance
  }

  get natureSpellResistance(): number {
    return this.options.spellResistance
  }

  get fireSpellResistance(): number {
    return this.options.spellResistance
  }

  get frostSpellResistance(): number {
    return this.options.spellResistance
  }

  get shadowSpellResistance(): number {
    return this.options.spellResistance
  }

  hasDebuff(debuff: Debuff): boolean {
    return this.options.debuffs.hasOwnProperty(debuff)
  }

  getDebuffStacks(debuff: Debuff): number {
    const val = this.options.debuffs[debuff]

    // val keeps ending up as string when using v-model in form, not sure why
    return this.hasDebuff(debuff) ? parseInt(val.toString(), 10) : 0
  }

  getScorchVulnerabilityFireDmgBonus(magicSchool: MagicSchool): number {
    if (magicSchool !== MagicSchool.Fire) return 1
    return 1 + this.getDebuffStacks(Debuff.ScorchVulnerability) * 0.03
  }
}
