import clonedeep from 'lodash/cloneDeep'

// import math from 'mathjs'
const math = require('mathjs')
const stats = require('statsjs')

export default class Tools {
  static cumulativeChance(trials: number, chance: number, x: number): number {
    return 1 - stats.binomcdf(trials, chance, x)
  }

  static consecutiveChance(trials: number, chance: number, x: number): number {
    const sStart = math.zeros([x + 1, x + 1])
    sStart[0][0] = 1

    const T = math.zeros([x + 1, x + 1])
    for (let i = 0; i < x; i++) {
      T[0][i] = 1 - chance
      T[i + 1][i] = chance
    }

    T[x][x] = 1
    const sEnd = math.multiply(math.pow(T, trials), sStart)
    return sEnd.slice(-1)[0][0]
  }

  static triangularNumber(n: number) {
    return (n * (n + 1)) / 2
  }

  static RoundedString(num: number, decimals: number): string {
    return num.toLocaleString('en-US', {
      minimumFractionDigits: decimals,
      maximumFractionDigits: decimals
    })
  }

  static isMobile() {
    const mql = window.matchMedia('(max-width: 768px)')
    if (!mql.matches) {
      return true
    }

    return false
  }

  static CloneObject(o: any) {
    return clonedeep(o)
  }

  static isLetter(char: string) {
    return char.length === 1 && char.match(/[a-z]/i) ? true : false
  }

  static capitalize(s: string) {
    return s.charAt(0).toUpperCase() + s.slice(1)
  }

  static baseURL() {
    if (window.location.host.includes('localhost')) {
      return window.location.host + '/'
    } else {
      return `https://pepopo978.gitlab.io/lock-calc-turtle-wow/`
    }
  }

  static EncodeURI(str: string) {
    return str
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
      .replace(/=+$/, '')
  }

  static DecodeURI(str: string) {
    str = (str + '===').slice(0, str.length + (str.length % 4))
    return str.replace(/-/g, '+').replace(/_/g, '/')
  }

  static publicUrl(queryStr: string) {
    return `${Tools.baseURL()}?${queryStr}`
  }

  static gearUrl() {
    return '' // TODO fix
  }

  static optionFromURL(name: string): any {
    const uri = window.location.search.substring(1)
    const params = new URLSearchParams(uri)
    const value = params.get(name.toLowerCase())

    if (value === null) {
      return null
    }

    switch (name.toLowerCase()) {
      // TODO fix
      default:
        return value
    }
  }
}
