/**
 * At level 60, caster classes have some expected amount of Int that will put them at 5% spell crit.
 * For example, to have 5% crit at 60 a mage needs 286 Int.  A 60 mage also needs 59.5 int to gain
 * 1 additional spell crit.  286/59.5=4.8067 which is less than 5, meaning mages have a base spell
 * crit of 5-(286/59.5)=0.1933. Likewise, a Shaman needs 160 int @ 60 for 5% crit, and 59.2 int for
 * 1 crit.  160/59.2=2.703 -> 5-(160/59.2)=2.2973 base spell crit
 *
 * http://blue.cardplace.com/cache/wow-mage/1009382.htm
 * http://blue.cardplace.com/cache/wow-general/8532087.htm
 * http://blue.cardplace.com/cache/wow-mage/559324.htm
 *
 */

/**
 * Mages have a 0.2% base spell crit rate, and 1% crit per 59.5 int
 * Priests have a 0.8% base spell crit rate, and 1% crit per 59.5 int
 * Warlocks have a 1.7% base spell crit rate, and 1% crit per 60.6 int
 * Druids have a 1.8% base spell crit rate, and 1% crit per 60 int
 * Shaman have a 2.3% base spell crit rate, and 1% crit per 59.2 int
 * Paladins have 0% base, and 1% crit per 29.5 int
 * https://nostalrius.org/viewtopic.php?f=24&t=23591
 */

export const GLOBAL_COOLDOWN = 1.5
export const PLAYER_LEVEL_CAP = 60
export const SPELL_HIT_CAP = 99
export const EQUIPMENT_HIT_CAP = 16
export const SPELL_CRIT_CAP = 100
export const SPELL_HASTE_CAP = 100
export const BASE_SPELL_CRIT_LOCK = 3.18
export const BASE_SPELL_CRIT_MULTIPLIER = 1.5

export const UNEQUIPPED_ITEM_ID = -1
export const UNLOCKED_ITEM_ID = 0
export const DUMMY_LOCKED_ITEM_ID = 1

export const UNEQUIPPED_ENCHANT_ID = -1
export const UNLOCKED_ENCHANT_ID = 0
export const DUMMY_LOCKED_ENCHANT_ID = 1

export const INT_FOR_1_SPELL_CRIT_MAGE = 59.5
export const INT_FOR_1_SPELL_CRIT_PRIEST = 59.2
export const INT_FOR_1_SPELL_CRIT_WARLOCK = 60.5
export const INT_FOR_1_SPELL_CRIT_DRUID = 60
export const INT_FOR_1_SPELL_CRIT_SHAMAN = 59.5
export const INT_FOR_1_SPELL_CRIT_PALADIN = 54

export const HEAD_ITEM_KEY = 'hI'
export const HEAD_ENCHANT_KEY = 'hE'
export const NECK_ITEM_KEY = 'nI'
export const SHOULDER_ITEM_KEY = 'sI'
export const SHOULDER_ENCHANT_KEY = 'sE'
export const BACK_ITEM_KEY = 'bI'
export const BACK_ENCHANT_KEY = 'bE'
export const CHEST_ITEM_KEY = 'cI'
export const CHEST_ENCHANT_KEY = 'cE'
export const WRIST_ITEM_KEY = 'wrI'
export const WRIST_ENCHANT_KEY = 'wrE'
export const MAINHAND_ITEM_KEY = 'mI'
export const MAINHAND_ENCHANT_KEY = 'mE'
export const OFFHAND_ITEM_KEY = 'oI'
export const RANGED_ITEM_KEY = 'rI'
export const HANDS_ITEM_KEY = 'haI'
export const HANDS_ENCHANT_KEY = 'haE'
export const WAIST_ITEM_KEY = 'wI'
export const WAIST_ENCHANT_KEY = 'wE'
export const LEGS_ITEM_KEY = 'lI'
export const LEGS_ENCHANT_KEY = 'lE'
export const FEET_ITEM_KEY = 'fI'
export const FEET_ENCHANT_KEY = 'fE'
export const FINGER_ITEM_KEY = 'fiI'
export const FINGER2_ITEM_KEY = 'fi2I'
export const TRINKET_ITEM_KEY = 'tI'
export const TRINKET2_ITEM_KEY = 't2I'

// all durations scaled 100x to avoid floating point math
export const SCALING_FACTOR = 100

export const IMMOLATE_DURATION = 15 * SCALING_FACTOR
export const CORRUPTION_DURATION = 18 * SCALING_FACTOR
export const CURSE_OF_AGONY_DURATION = 24 * SCALING_FACTOR
export const CURSE_OF_SHADOW_DURATION = 300 * SCALING_FACTOR

export const IMMOLATE_MIN_TIME_LEFT_FOR_RECAST = 12 * SCALING_FACTOR
export const CORRUPTION_MIN_TIME_LEFT_FOR_RECAST = 15 * SCALING_FACTOR
export const CURSE_OF_AGONY_MIN_TIME_LEFT_FOR_RECAST = 20 * SCALING_FACTOR
export const CURSE_OF_SHADOW_MIN_TIME_LEFT_FOR_RECAST = 10 * SCALING_FACTOR

export const IMMOLATE_TICK_SP_COEFFICIENT = 0.1272
export const CORRUPTION_TICK_SP_COEFFICIENT = 0.1666
export const CURSE_OF_AGONY_TICK_SP_COEFFICIENT = 0.0833

export const IMMOLATE_TICK_DAMAGE = 102
export const CORRUPTION_TICK_DAMAGE = 137
export const CURSE_OF_AGONY_TICK_DAMAGE = 87

export const IMMOLATE_DIRECT_DAMAGE = 279
export const IMMOLATE_DIRECT_DAMAGE_SP_COEFFICIENT = 0.1865
export const CURSE_OF_AGONY_NUM_TICKS = 12

export const IMP_CRIT_CHANCE = .05 // estimated
