export const SMRuin = {
  affliction: {
    suppression: 3,
    improvedCorruption: 5,

    improvedCurseOfWeakness: 0,
    improvedDrainSoul: 0,
    improvedLifeTap: 2,
    soulSiphon: 2,

    improvedCurseOfAgony: 3,
    felConcentration: 0,
    amplifyCurse: 1,

    grimReach: 2,
    nightfall: 2,

    siphonLife: 1,
    curseOfExhaustion: 1,
    improvedCurseOfExhaustion: 3,

    shadowMastery: 5,

    darkPact: 0
  },
  demonology: {
    masterConjurer: 0,
    improvedImp: 0,
    demonicEmbrace: 0,

    improvedHealthFunnel: 0,
    improvedVoidwalker: 0,
    felIntellect: 0,

    improvedSuccubus: 0,
    felDomination: 0,
    felStamina: 0,
    demonicAegis: 0,

    masterSummoner: 0,
    unholyPower: 0,

    improvedEnslaveDemon: 0,
    demonicSacrifice: 0,

    masterDemonologist: 0,

    soulLink: 0
  },
  destruction: {
    improvedShadowBolt: 5,
    cataclysm: 0,

    bane: 5,
    aftermath: 0,

    improvedFirebolt: 0,
    improvedLashOfPain: 0,
    devastation: 5,
    shadowburn: 1,

    intensity: 2,
    destructiveReach: 2,
    improvedSearingPain: 0,

    pyroclasm: 0,
    improvedImmolate: 0,
    ruin: 1,

    emberstorm: 0,

    conflagrate: 0
  }
}

export const DSRuin = {
  affliction: {
    suppression: 2,
    improvedCorruption: 5,

    improvedCurseOfWeakness: 0,
    improvedDrainSoul: 0,
    improvedLifeTap: 2,
    soulSiphon: 0,

    improvedCurseOfAgony: 0,
    felConcentration: 0,
    amplifyCurse: 0,

    grimReach: 0,
    nightfall: 0,

    siphonLife: 0,
    curseOfExhaustion: 0,
    improvedCurseOfExhaustion: 0,

    shadowMastery: 0,

    darkPact: 0
  },
  demonology: {
    masterConjurer: 2,
    improvedImp: 0,
    demonicEmbrace: 5,

    improvedHealthFunnel: 0,
    improvedVoidwalker: 3,
    felIntellect: 0,

    improvedSuccubus: 0,
    felDomination: 1,
    felStamina: 3,
    demonicAegis: 3,

    masterSummoner: 2,
    unholyPower: 1,

    improvedEnslaveDemon: 0,
    demonicSacrifice: 1,

    masterDemonologist: 0,

    soulLink: 0
  },
  destruction: {
    improvedShadowBolt: 5,
    cataclysm: 0,

    bane: 5,
    aftermath: 0,

    improvedFirebolt: 0,
    improvedLashOfPain: 0,
    devastation: 5,
    shadowburn: 1,

    intensity: 2,
    destructiveReach: 2,
    improvedSearingPain: 0,

    pyroclasm: 0,
    improvedImmolate: 0,
    ruin: 1,

    emberstorm: 0,

    conflagrate: 0
  }
}
