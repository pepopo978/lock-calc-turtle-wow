enum TalentSchool {
  Affliction,
  Demonology,
  Destruction
}

export default TalentSchool
