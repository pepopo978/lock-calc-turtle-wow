enum Debuff {
  CurseOfShadow = 'a',
  CurseOfElements = 'b',
  SpellVulnerability = 'c',
  WintersChill = 'd',
  ScorchVulnerability = 'e',
  Ignite = 'f'
}

export default Debuff
