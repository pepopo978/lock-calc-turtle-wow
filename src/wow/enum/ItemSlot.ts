/* These slot id's are based on wowheads XML. They don't necessarily match
 * the in-game slot id */

enum ItemSlot {
  Head = 'Head',
  Neck = 'Neck',
  Shoulder = 'Shoulder',
  Chest = 'Chest',
  Waist = 'Waist',
  Legs = 'Legs',
  Feet = 'Feet',
  Wrist = 'Wrist',
  Hands = 'Hands',
  Finger = 'Finger',
  Finger2 = 'Finger2',
  Trinket = 'Trinket',
  Trinket2 = 'Trinket2',
  Ranged = 'Ranged',
  Back = 'Back',
  Onehand = 'Onehand',
  Twohand = 'Twohand',
  Mainhand = 'Mainhand',
  Offhand = 'Offhand'
}

export default ItemSlot

export function getEmptySlotIcon(slot: ItemSlot) {
  return process.env.BASE_URL + 'wow-icons/' + slot.replace('2', '') + '.jpg'
}
