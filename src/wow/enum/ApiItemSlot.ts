import ItemSlot from './ItemSlot'

enum ApiItemSlot {
  Head = 0,
  Neck = 1,
  Shoulder = 2,
  Tunic = 3,
  Chest = 4,
  Waist = 5,
  Legs = 6,
  Feet = 7,
  Wrist = 8,
  Hands = 9,
  Finger = 10,
  Finger2 = 11,
  Trinket = 12,
  Trinket2 = 13,
  Back = 14,
  Mainhand = 15,
  Offhand = 16,
  Ranged = 17
}

export default ApiItemSlot

export function getItemSlotForApiSlot(slot: ApiItemSlot, itemSubClass?: number) {
  switch (slot) {
    case ApiItemSlot.Head:
      return ItemSlot.Head
    case ApiItemSlot.Neck:
      return ItemSlot.Neck
    case ApiItemSlot.Shoulder:
      return ItemSlot.Shoulder
    case ApiItemSlot.Chest:
      return ItemSlot.Chest
    case ApiItemSlot.Waist:
      return ItemSlot.Waist
    case ApiItemSlot.Legs:
      return ItemSlot.Legs
    case ApiItemSlot.Feet:
      return ItemSlot.Feet
    case ApiItemSlot.Wrist:
      return ItemSlot.Wrist
    case ApiItemSlot.Hands:
      return ItemSlot.Hands
    case ApiItemSlot.Finger:
      return ItemSlot.Finger
    case ApiItemSlot.Finger2:
      return ItemSlot.Finger2
    case ApiItemSlot.Trinket:
      return ItemSlot.Trinket
    case ApiItemSlot.Trinket2:
      return ItemSlot.Trinket2
    case ApiItemSlot.Ranged:
      return ItemSlot.Ranged
    case ApiItemSlot.Back:
      return ItemSlot.Back
    case ApiItemSlot.Mainhand:
      // staff is 10, twohanded
      if (itemSubClass === 10) {
        return ItemSlot.Twohand
      }
      // otherwise should be 1h
      return ItemSlot.Mainhand
    case ApiItemSlot.Offhand:
      return ItemSlot.Offhand
  }

  throw new Error('Unknown slot ' + slot)
}
